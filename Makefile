#.PHONY: clean

FLAGS = -g

ans: ar.o prints.o lengthes.o func.o ans.o encdec.o main.o fse_compress.o hist.o
	gcc $(FLAGS) ar.o prints.o lengthes.o func.o ans.o encdec.o main.o fse_compress.o hist.o -lm -o $@

clean:
	rm -f ans *.o

ar.o: ar.c
	gcc $(FLAGS) -c ar.c -o $@
prints.o: prints.c
	gcc $(FLAGS) -c prints.c -o $@
lengthes.o: lengthes.c
	gcc $(FLAGS) -c lengthes.c -o $@
func.o: func.c
	gcc $(FLAGS) -c func.c -o $@
ans.o: ans.c
	gcc $(FLAGS) -c ans.c -o $@
encdec.o: encdec.c
	gcc $(FLAGS) -c encdec.c -o $@
main.o: main.c
	gcc $(FLAGS) -c main.c -o $@
# fse_normalization.o: fse_normalization.c
# 	gcc $(FLAGS) -c fse_normalization.c -o $@
# fse_1st_attempt.o: fse_1st_attempt.c
# 	gcc $(FLAGS) -c fse_1st_attempt.c -o $@
fse_compress.o: fse_compress.c
	gcc $(FLAGS) -c fse_compress.c -o $@
hist.o: hist.c
	gcc $(FLAGS) -c hist.c -o $@


