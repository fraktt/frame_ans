#include "ans.h"
// #include "fse_normalization.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

void ezcs_CollectStatistics(int *ezcsFreq, const Dict* dict, int PROBABILITY_PRECISION) {
	int nSymbols = dict->len;

	//casting frequencies
	double coef = (double)(1 << PROBABILITY_PRECISION) / dict->sumOfFreqs;
	int total = 0; //sum of casted freqs
	for (int i = 0; i < nSymbols; ++i) {
		ezcsFreq[i] = (int)((double)(ezcsFreq[i]) * coef);
		total += ezcsFreq[i];
	}
	int diff = (1 << PROBABILITY_PRECISION) - total; // = 2^(PROB_PREC) - total

	//searching max and maxPos in freq[]
	int maxPos = 0;
	int max = ezcsFreq[maxPos];
	for (int i = 0; i < nSymbols; ++i) {
		if (ezcsFreq[i] > max) {
			max = ezcsFreq[i];
			maxPos = i;
		}
	}

	ezcsFreq[maxPos] += diff; //to make sum of freqs == (1 << PROBABILITY_PRECISION)

	//distribution freqs among zeros (zeros could appear during casting frequencies)
	for (int i = 0; i < nSymbols; ++i) {
		if (ezcsFreq[i] == 0) {
			ezcsFreq[i] = 1;
			--ezcsFreq[maxPos];
		}
	}
}

void ezcs_CollectStatistics_2(int *ezcsFreq, const Dict* dict, int PROBABILITY_PRECISION) {
	double coef = (double)(1 << PROBABILITY_PRECISION) / dict->sumOfFreqs;
	int maxPos = 0;
	int max = 0; //ezcsFreq[maxPos];
	int total = 0; //sum of all normalized freqs

	for (int i = 0; i < dict->len; ++i) {
		//здесь можно округлять к ближайшему целому, а не приводить к int (округление вниз) /////////////////////////
		ezcsFreq[i] = (int)((double)(dict->syms[i].freq) * coef); //casting (normalization) frequencies
		if (ezcsFreq[i] == 0) { //(zeros could appear during casting frequencies)
			ezcsFreq[i] = 1; 
		}

		if (ezcsFreq[i] > max) { //searching max and maxPos
			maxPos = i;
			max = ezcsFreq[maxPos];
		}

		total += ezcsFreq[i]; //sum of casted freqs
	}

	//элемент на позиции maxPos может перестать быть максимальным /////////////////////////
	ezcsFreq[maxPos] += (1 << PROBABILITY_PRECISION) - total; //to make sum of freqs = (1 << PROB_PREC) = 2^(PROB_PREC)
}

void GetOrder(int *data, int *order, int size) {
	int min = data[0];
	int max = data[0];
	for (int i = 0; i < size; ++i) { //searching min and max in data[]
		if (min > data[i]) min = data[i];
		if (max < data[i]) max = data[i];
	}
	if (min == max) { // data[] == zeros
		for (int i = 0; i < size; ++i) {
			order[i] = i;
		}
		return;
	}
	++max; //now max is bigger than any element of data[] //analog Inf

	//copying  data -> tmp
	int tmp[size];
	memcpy(tmp, data, size * sizeof(int)); //data -> tmp

	for (int k = 0; k < size; ++k) {
		int minPos = 0;
		min = tmp[minPos];
		for (int i = 0; i < size; ++i) { //searching min and minPos in tmp[]
			if (min > tmp[i]) {
				min = tmp[i];
				minPos = i;
			}
		}
		order[k] = minPos; //writing to order[]
		tmp[minPos] = max; //as Inf //so will not be min later
	}
}

void ezcs_MakeChart(int *chart, int *ezcsFreq, int *sizes, int nSymbols, int PROBABILITY_PRECISION, int MAX_STATE) {
	//sizes[] = {1,...,1}
	for (int i = 0; i < nSymbols; ++i) {
		sizes[i] = 1;  //this is because table starts from 1, not from 0
	}

	//getting order[] - indexes of freq[] in ascending order (from min to max)
	int order[nSymbols];
	GetOrder(ezcsFreq, order, nSymbols); 
	//printOrder(order, nSymbols); ////////////////

	int denominator = 1 << PROBABILITY_PRECISION;
	for (int i = 0; i <= MAX_STATE; ++i) { //initial states
		chart[i] = denominator;
	}
	for (int j = 1; j <= MAX_STATE; ++j) {
		for (int i = 0; i < nSymbols; ++i) {
			int currSymNum = order[i]; //current symbol (from min freq to max freq)
			uint64_t state = ((uint64_t)j << PROBABILITY_PRECISION) / ezcsFreq[currSymNum];
			if (state < 2) state = 2;
			while (state <= MAX_STATE) {
				if (chart[state] == denominator) { // == (initial state) (first visiting of state)
					chart[state] = currSymNum; //first the least frequent
					++sizes[currSymNum]; //increasing length of the row in the table
					break;
				}
				++state; //next state
			}
		}
	}
}

void ezcs_MakeChart_dict(int *chart, Dict sortNormDict, int *sizes, int PROBABILITY_PRECISION, int MAX_STATE) {
	int nSymbols = sortNormDict.len;
	for (int i = 0; i < nSymbols; ++i) { //sizes[] = {1,...,1}
		sizes[i] = 1;  //this is because table starts from 1, not from 0
	}

	int denominator = 1 << PROBABILITY_PRECISION;
	for (int i = 0; i <= MAX_STATE; ++i) { //initial states
		chart[i] = denominator;
	}

	for (int j = 1; j <= MAX_STATE; ++j) {

		uint64_t nmrtr = (uint64_t)j << PROBABILITY_PRECISION; //numerator
		
		for (int i = 0; i < nSymbols; ++i) {
			Sym *sym = &(sortNormDict.syms[i]);

			uint64_t state = nmrtr / sym->freq; //setting start state
			if (state < 2) {
				state = 2;
			}

			while (state <= MAX_STATE) {
				if (chart[state] == denominator) { // == (initial state) (first visiting of state)
					chart[state] = sym->num; //first the least frequent
					++sizes[sym->num]; //increasing length of the row in the table
					break;
				}
				++state; //next state
			}
		}
	}
}

// void makeChart_fseSpread_dict(int *chart, Dict sortNormDict, int *sizes, int PROBABILITY_PRECISION, int MAX_STATE) {
// //int chart[MAX_STATE + 1] = [1 << STATE_PRECISION - 1 + 1] = [1 << STATE_PRECISION] = [1 << (PROBABILITY_PRECISION + 1)]
// 	short normalizedCounter[sortNormDict.len];
// 	for (int i = 0; i < sortNormDict.len; ++i) {
// 		assert(sortNormDict.syms[i].freq < 65536); //чтобы уместилось в short
// 		normalizedCounter[sortNormDict.syms[i].num] = sortNormDict.syms[i].freq;
// 	}

// 	fseSpread(normalizedCounter, sortNormDict.len - 1, PROBABILITY_PRECISION, chart, MAX_STATE + 1);
// }


void makeDirectTable(int **directTable, int *chart, int *sizes, int nSymbols, int PROBABILITY_PRECISION, int MAX_STATE) {
	int denominator = 1 << PROBABILITY_PRECISION;
	int order[nSymbols];
	for (int i = 0; i < nSymbols; ++i) { //order[] = {1,...,1} //first states for all symbols are ones (1)
		order[i] = 1;
	}

	for (int j = 2; j <= MAX_STATE; ++j) {
		int symbol = chart[j];
		if (symbol != denominator) { // != 2^PROBABILITY_PRECISION (initial)
			//j - new state
			//order[symbol] - next value of prev state for this symbol
			assert(symbol < nSymbols); ///////////////////////
			
			if (order[symbol] >= sizes[symbol]) {
				printf("%i %i\n", order[symbol], sizes[symbol]); //////////////////////
				assert(order[symbol] < sizes[symbol]); ///////////////////////
			}
			
			directTable[symbol][order[symbol]++] = j;
		}
	}
}

int testDirectTable(int **directTable, int *sizes, int  nSymbols, int STATE_PRECISION) {
	int MASK = 1 << (STATE_PRECISION - 1);
	for (int i = 0; i < nSymbols; ++i) {
		int smallestStateOutOfRange = sizes[i];
		if (directTable[i][smallestStateOutOfRange >> 1] < MASK) {
			return -1;
		}
	}
	return 0;
}



int nNonZeroFreqs(int *freqs, int freqsLen) {
	int nSymbols = 0;
	for (int i = 0; i < freqsLen; ++i) {
		if (freqs[i] != 0) {
			++nSymbols;
		}
	}
	return nSymbols;
}

void makeInverseTable(int **directTable, InvTab *inverseTable, int nSymbols, int *sizes) {
	inverseTable[0].state = 0;
	inverseTable[0].symbol = 0;
	for (int i = 0; i < nSymbols; ++i) {
		//previous state + symbol -> current state
		//i - symbol
		//j - previous state
		//directTable[i][j] - current state
		for (int j = 1; j < sizes[i]; ++j) {
			inverseTable[directTable[i][j]].symbol = i;
			inverseTable[directTable[i][j]].state = j;
		}
	}
}

int readSymbolNum(dir_t dir, const Ints *nums, int nRead) {
	if (dir == dir_backw) { //direct reading
		return nums->vals[nRead];
	}
	else /*if (dir == dir_forw)*/ { //inverse reading
		return nums->vals[nums->len - 1 - nRead];
	}
}

void writeSymbolNum(dir_t dir, Ints *nums, int *nWrittenSyms, int num) {
	if (dir == dir_backw) { //inverse writing
		nums->vals[nums->len - 1 - ((*nWrittenSyms)++)] = num;
	}
	else if (dir == dir_forw) { //dir == 2 //direct writing
		nums->vals[(*nWrittenSyms)++] = num;
	}
}

void writeFinalState(uint8_t *outputData, int *nWrittenBytes, Bits *buf, int state, int STATE_PRECISION) {
	while (state > 0) {
		addBitToBits(buf, state & 0x1);
		if (buf->len == BYTE_LEN) {
			outputData[(*nWrittenBytes)++] = (uint8_t)buf->data;
			buf->len = 0;
		}
		state >>= 1;
	}
}


void prepareDirectTable(int nSymbols, const Dict* normDict, int PROBABILITY_PRECISION, int *sizes, int **directTable) {
	const int STATE_PRECISION = GET_STATE_PRECISION(PROBABILITY_PRECISION);
	const int MAX_STATE = GET_MAX_STATE(STATE_PRECISION);
	
	//Make CHART
	Dict sortNormDict;
	copyDict(normDict, &sortNormDict);
	sortDict_getOrder(&sortNormDict);
	int chart[MAX_STATE + 1];
	memset(chart, 0, sizeof(chart)); ////////////////
	#if 1
	ezcs_MakeChart_dict(chart, sortNormDict, sizes, PROBABILITY_PRECISION, MAX_STATE);	
	#else
	makeChart_fseSpread_dict(chart, sortNormDict, sizes, PROBABILITY_PRECISION, MAX_STATE);
	#endif
	// printChart(chart, sizes, MAX_STATE, nSymbols); ///////////////////

	//make direct table
	for (int i = 0; i < nSymbols; ++i) {
		directTable[i] = malloc((sizes[i]) * sizeof(int)); //allocating memory
		memset((directTable)[i], 0x00, (sizes[i]) * sizeof(int)); //filling with zeros
	}
	makeDirectTable(directTable, chart, sizes, nSymbols, PROBABILITY_PRECISION, MAX_STATE);
	
	//printDirectTable(directTable, sizes, nSymbols); /////////////////
	
	free(sortNormDict.syms);
}

int readFinalState(Bytes *encData, int MASK) {
	int state = 0;
	while (state < MASK) {
	// for (int i = 0; i < bitlen(MASK); ++i) {
		state <<= 1;
		state |= readBitFromBytes(encData);
	}
	return state;
}

void readFinalState_direct(Bytes *bts, int MASK, int *state) {
	*state = 0;
	while (*state < MASK) {
	// for (int i = 0; i < bitlen(MASK); ++i) {
		*state <<= 1;
		*state |= readBitFromBytes(bts);
	}
}


int readContFlag(dir_t dir, const Ints *contFlags, int nRead) {
	if (dir == dir_backw) { //direct reading
		return contFlags->vals[nRead];
	}
	else /*if (dir == dir_forw)*/ { //inverse reading
		return contFlags->vals[contFlags->len - 1 - nRead];
	}
}


void createDirectTables(int **sizeses, int ***directTables, Dict *dicts, int nConts) {
	//dicts should be already normalized

	int PROBABILITY_PRECISION = probPrec();
	int STATE_PRECISION = GET_STATE_PRECISION(PROBABILITY_PRECISION); //number of bits for state

	for (int cont = 0; cont < nConts; ++cont) {
		int nSymbols = dicts[cont].len;
		if (nSymbols > 1) { 
			sizeses[cont] = malloc(nSymbols * sizeof(int)); //sizes[nSymbols]
			directTables[cont] = malloc(nSymbols * sizeof(int*)); //directTable[nSymbols][]
			prepareDirectTable(nSymbols, &(dicts[cont]), PROBABILITY_PRECISION, sizeses[cont], directTables[cont]);
		}

		// if (testDirectTable(directTables[cont], sizeses[cont], nSymbols, STATE_PRECISION) != 0) {
		// 	printf("Bad direct table %i\n", cont);
		// 	printDirectTable(directTables[cont], sizeses[cont], nSymbols);
		// 	exit(1);
		// }
	}
}



void freeDirectTables(int nConts, Dict *dicts, int **sizeses, int ***directTables) {
	for (int cont = 0; cont < nConts; cont++) {
		if (dicts[cont].len > 1) {
			for (int i = 0; i < dicts[cont].len; ++i) {
				free(directTables[cont][i]);
			}
			free(directTables[cont]);
			free(sizeses[cont]);
		}
	}
}



void createInverseTables(Dict *dicts, int nConts, int **sizeses, int ***directTables, InvTab **inverseTables) {
	int PROBABILITY_PRECISION = probPrec();
	int STATE_PRECISION = GET_STATE_PRECISION(PROBABILITY_PRECISION); //number of bits for state
	int MAX_STATE = GET_MAX_STATE(STATE_PRECISION);
	for (int cont = 0; cont < nConts; ++cont) {
		if (dicts[cont].len > 1) {
			inverseTables[cont] = malloc((MAX_STATE + 1) * sizeof(InvTab));
			makeInverseTable(directTables[cont], inverseTables[cont], dicts[cont].len, sizeses[cont]);
			//printInverseTable((*inverseTables)[cont], MAX_STATE + 1); ////////////////////
		}
	}
}


void freeInverseTables(int nConts, Dict *dicts, InvTab **inverseTables) {
	for (int cont = 0; cont < nConts; ++cont) {
		if (dicts[cont].len > 1) {
			free(inverseTables[cont]);
		}
	}
}

int ANS_enc_proc_old(const Ints* nums, dir_t dir, int *sizes, int **directTable, uint8_t *outputData, int STATE_PRECISION) {
	int MAX_STATE = GET_MAX_STATE(STATE_PRECISION);
	int MASK = 1 << (STATE_PRECISION - 1);

	int state = MAX_STATE;
	Bits buf = { 0, 0 };
	int nRead = 0;
	int nWrittenBytes = 0;

	while (nRead < nums->len) {
		int symbolNum = readSymbolNum(dir, nums, nRead); //getting symbol
		++nRead;
		while (state > sizes[symbolNum] - 1) { //bigger than hight of the symbol's column in the table
			addBitToBits(&buf, state & 1);
			state >>= 1;
			if (buf.len == BYTE_LEN) {
				outputData[nWrittenBytes++] = (uint8_t)buf.data;
				buf.data = 0;
				buf.len = 0;
			}
		}
		state = directTable[symbolNum][state]; //new state
		if (state < MASK) {
			printf("problem with data symbolNum %d %d\n", symbolNum, state);
			return -1;
		}
	}

	writeFinalState(outputData, &nWrittenBytes, &buf, state, STATE_PRECISION); //writing last state (whole)

	//zero padding
	int nZeros = BYTE_LEN - buf.len;
	if (buf.len != 0) {
		buf.data <<= nZeros;
		outputData[nWrittenBytes++] = (uint8_t)buf.data;
	}

	return nWrittenBytes;
}

void ANS_enc_cont_proc(const Ints *nums, const Ints *contFlags, dir_t dir, int **sizeses, int ***directTables, Bytes *output, int nConts, Dict *dicts) {
	int PROBABILITY_PRECISION = probPrec();
	int STATE_PRECISION = GET_STATE_PRECISION(PROBABILITY_PRECISION); //number of bits for state
	int MAX_STATE = GET_MAX_STATE(STATE_PRECISION);
	int MASK = 1 << (STATE_PRECISION - 1);
	// int state = MAX_STATE;
	int state = MASK;

	for (int i = 0; i < nums->len; ++i) {
		int cont = readContFlag(dir, contFlags, i); 
		int *sizes = sizeses[cont];
		int **directTable = directTables[cont];
		int symbolNum = readSymbolNum(dir, nums, i);
		while (state > sizes[symbolNum] - 1) { //bigger than hight of the symbol's column in the table
			addBitToBytes(output, state);
			state >>= 1;
		}
		state = directTable[symbolNum][state]; //new state
		if (state < MASK) {
			printf("problem with data symbolNum %d %d\n", symbolNum, state);
			exit(1);
		}
	}
	while (state > 0) { //writing final state //writing last state (whole)
		addBitToBytes(output, state);
		state >>= 1;
	}
}



void ANS_enc_cont_proc_reverse(const Ints* nums, const Ints *contFlags, dir_t dir, int **sizeses, int ***directTables, Bytes *out, int nConts, Dict *dicts) {
	int PROBABILITY_PRECISION = probPrec();
	int STATE_PRECISION = GET_STATE_PRECISION(PROBABILITY_PRECISION); //number of bits for state
	int MAX_STATE = GET_MAX_STATE(STATE_PRECISION);
	int MASK = 1 << (STATE_PRECISION - 1);
	int state = MASK;
	Bytes intermediate = allocBytesLen(BYTES_ALLOC_STEP); //bytes in direct order, bits in reverse order (inside byte)
	// printDirectTable(directTables[0], sizeses[0], dicts[0].len);///////////////////////////

	for (int i = 0; i < nums->len; ++i) {
		int cont = readContFlag(dir, contFlags, i); 
		
		if (dicts[cont].len == 1) { //контекст содержит 1 символ 
			continue;
		}
		else {
			int *sizes = sizeses[cont];
			int **directTable = directTables[cont];
			int symbolNum = readSymbolNum(dir, nums, i);

			while (state > sizes[symbolNum] - 1) { //bigger than hight of the symbol's column in the table
				addBitToBytes_reverse(&intermediate, state);
				state >>= 1;
			}
			state = directTable[symbolNum][state]; //new state

			assert(state >= MASK); //if (state < MASK) printf("problem with data symbolNum %d %d\n", symbolNum, state);
		}	
	}
	
	int encDataExists = 0;
	for (int i = 0; i < nConts; ++i) {
		if (dicts[i].len > 1) {
			encDataExists = 1;
			break;
		}
	}
	if (encDataExists) { //если суббэнда не была нулевой
		while (state > 0) { //writing final state //writing last state (whole)
			addBitToBytes_reverse(&intermediate, state);
			state >>= 1;
		}
	}

	int nBytes = getRealBytesLen(intermediate);
	for (int i = 0; i < nBytes; ++i) { //turn bytes to the reverse order (as bits)
		// addByteToBytes(out, intermediate.bytes[nBytes - 1 - i]);
		addByteToBytes(out, intermediate.bytes[nBytes - 1 - i]);
	}
	freeBytes(&intermediate);
}


void ANS_enc(const Ints *nums, const Ints *contFlags, Dict *dicts, Bytes *out, const ExtMode* md, int nConts) {
	int *sizeses[nConts];
	int **directTables[nConts];

	struct timespec start, end;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	createDirectTables(sizeses, directTables, dicts, nConts);
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	timespec_add(&md->times->time_enc_createDirectTable, timespec_sub(end, start));

	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	ANS_enc_cont_proc_reverse(nums, contFlags, md->dir, sizeses, directTables, out, nConts, dicts); //encoding
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	timespec_add(&md->times->time_enc_proc, timespec_sub(end, start));

	freeDirectTables(nConts, dicts, sizeses, directTables);
}


void ANS_dec_cont_proc_gr(Bytes encData, Group *gr, Subband *sbs, InvTab **inverseTables, Dict *dicts, md_t mode) {
	int PROBABILITY_PRECISION = probPrec();
	int STATE_PRECISION = GET_STATE_PRECISION(PROBABILITY_PRECISION);
	int MASK = 1 << (STATE_PRECISION - 1);

	encData.posByte = encData.capacity - 1;
	encData.posBit = 7; //LSB
	int state = readFinalState(&encData, MASK);

	for (int sb_i = 0; sb_i < gr->sbNums.len; ++sb_i) {
		int sbNum = gr->sbNums.vals[sb_i];

		Subband *sb = &(sbs[sbNum]);
		for (int row = 0; row < sb->matr.nRows; ++row) {
			for (int col = 0; col < sb->matr.nColumns; ++col) {

				int cont = getContext(sb->matr, row, col, gr->cm);
				if (gr->cm.isParentUsage && gr->sbsType == sb_1) {
					considerParent_dec(&cont, sb, row, col, gr->cm.pType, mode);
				}

				InvTab *inverseTable = inverseTables[cont]; //choosing inverse table according to context

				int num = inverseTable[state].symbol; //x' -> num //output of decoder
				state = inverseTable[state].state; //x' -> x
				int sym = dicts[cont].syms[num].sym; //num -> sym
				sb->matr.matrix[row][col] = sym;

				while (state < MASK) { //bringing the state to a large value
					state <<= 1;
					state |= readBitFromBytes(&encData); //adding a bit to the state	
				}
			}
		}
	}
}


void ANS_dec_cont_proc_gr_reverse(Bytes *bts, Group *gr, Subband *sbs, InvTab **inverseTables, Dict *dicts, md_t mode) {
	int PROBABILITY_PRECISION = probPrec();
	int STATE_PRECISION = GET_STATE_PRECISION(PROBABILITY_PRECISION);
	int MASK = 1 << (STATE_PRECISION - 1);

	int state;
	
	int encDataExists = 0;
	for (int i = 0; i < gr->nConts; ++i) {
		if (dicts[i].len > 1) {
			encDataExists = 1;
			break;
		}
	}
	
	if (encDataExists) {
		readFinalState_direct(bts, MASK, &state);
	}

	for (int sb_i = 0; sb_i < gr->sbNums.len; ++sb_i) {
		int sbNum = gr->sbNums.vals[sb_i];

		Subband *sb = &(sbs[sbNum]);
		for (int row = 0; row < sb->matr.nRows; ++row) {
			for (int col = 0; col < sb->matr.nColumns; ++col) {
				int cont = getContext(sb->matr, row, col, gr->cm);
				if (gr->cm.isParentUsage && gr->sbsType == sb_1) {
					considerParent_dec(&cont, sb, row, col, gr->cm.pType, mode);
				}

				if (dicts[cont].len == 1) {
					assert(dicts[cont].syms[0].freq == dicts[cont].sumOfFreqs); ////////////////
					sb->matr.matrix[row][col] = dicts[cont].syms[0].sym; //num -> sym 
				}

				else {
					InvTab *inverseTable = inverseTables[cont]; //choosing inverse table according to context

					int num = inverseTable[state].symbol; //x' -> num //output of decoder
					state = inverseTable[state].state; //x' -> x
					int sym = dicts[cont].syms[num].sym; //num -> sym
					sb->matr.matrix[row][col] = sym;

					while (state < MASK) { //bringing the state to a large value
						state <<= 1;
						state |= readBitFromBytes(bts); //adding a bit to the state	
					}
				}
			}
		}
	}
}



void ANS_dec_cont_proc_sb_reverse(Bytes *bts, Subband *sb, InvTab **inverseTables, Dict *dicts, md_t mode) {
	int PROBABILITY_PRECISION = probPrec();
	int STATE_PRECISION = GET_STATE_PRECISION(PROBABILITY_PRECISION);
	int MASK = 1 << (STATE_PRECISION - 1);

	int state = 0;

	int encDataExists = 0;
	for (int i = 0; i < sb->nConts; ++i) {
		if (dicts[i].len > 1) {
			encDataExists = 1;
			break;
		}
	}

	if (encDataExists) {
		readFinalState_direct(bts, MASK, &state);
	}

	for (int row = 0; row < sb->matr.nRows; ++row) {
		for (int col = 0; col < sb->matr.nColumns; ++col) {
			int cont = getContext(sb->matr, row, col, sb->cm);
			if (sb->cm.isParentUsage && sb->sbType == sb_1) {
				considerParent_dec(&cont, sb, row, col, sb->cm.pType, mode);
			}

			if (dicts[cont].len == 1) {
				assert(dicts[cont].syms[0].freq == dicts[cont].sumOfFreqs); ////////////////
				sb->matr.matrix[row][col] = dicts[cont].syms[0].sym; //num -> sym 
			}

			else {
				InvTab *inverseTable = inverseTables[cont]; //choosing inverse table according to context

				int num = inverseTable[state].symbol; //x' -> num //output of decoder
				state = inverseTable[state].state; //x' -> x
				int sym = dicts[cont].syms[num].sym; //num -> sym
				sb->matr.matrix[row][col] = sym;

				while (state < MASK) { //bringing the state to a large value
					state <<= 1;
					state |= readBitFromBytes(bts); //adding a bit to the state	
				}
			}
		}
	}
}


void ANS_dec_gr(Dict *dicts, Bytes encData, Group *gr, Subband *sbs, md_t mode) {
	//dicts should be already normalized

	int nConts = gr->nConts;

	int *sizeses[nConts];
	int **directTables[nConts];
	createDirectTables(sizeses, directTables, dicts, nConts);

	InvTab *inverseTables[nConts];
	createInverseTables(dicts, nConts, sizeses, directTables, inverseTables);

	ANS_dec_cont_proc_gr(encData, gr, sbs, inverseTables, dicts, mode);

	freeDirectTables(nConts, dicts, sizeses, directTables);
	freeInverseTables(nConts, dicts, inverseTables);
}

void ANS_dec_gr_reverse(Dict *dicts, Bytes *bts, Group *gr, Subband *sbs, md_t mode) {
	//dicts should be already normalized

	int nConts = gr->nConts;

	int *sizeses[nConts];
	int **directTables[nConts];
	createDirectTables(sizeses, directTables, dicts, nConts);

	InvTab *inverseTables[nConts];
	createInverseTables(dicts, nConts, sizeses, directTables, inverseTables);

	ANS_dec_cont_proc_gr_reverse(bts, gr, sbs, inverseTables, dicts, mode);

	freeDirectTables(nConts, dicts, sizeses, directTables);
	freeInverseTables(nConts, dicts, inverseTables);
}



void ANS_dec_sb_reverse(Dict *dicts, Bytes *bts, Subband *sb, const ExtMode* md) {
	//dicts should be already normalized

	int nConts = sb->nConts;

	int *sizeses[nConts];
	int **directTables[nConts];
	InvTab *inverseTables[nConts];

	struct timespec start, end;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	createDirectTables(sizeses, directTables, dicts, nConts);
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	timespec_add(&md->times->time_dec_createDirectTable, timespec_sub(end, start));

	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	createInverseTables(dicts, nConts, sizeses, directTables, inverseTables);
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	timespec_add(&md->times->time_dec_createInverseTable, timespec_sub(end, start));

	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	ANS_dec_cont_proc_sb_reverse(bts, sb, inverseTables, dicts, md->mode);
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	timespec_add(&md->times->time_dec_proc, timespec_sub(end, start));


	freeDirectTables(nConts, dicts, sizeses, directTables);
	freeInverseTables(nConts, dicts, inverseTables);
}


