#pragma once

#include <stdlib.h>
#include <stdio.h>

#include "constsAndTypes.h"
#include "prints.h"
#include "constsAndTypes.h"
#include "func.h"


void ezcs_CollectStatistics_2(int *ezcsFreq, const Dict* dict, int PROBABILITY_PRECISION);
void GetOrder(int *data, int *order, int size);

void ANS_enc(const Ints* nums, const Ints* contFlags, Dict* dicts, Bytes *out, const ExtMode* md, int nConts);

void ANS_dec_gr(Dict* dicts, Bytes encData, Group *gr, Subband *sbs, md_t mode);
void ANS_dec_gr_reverse(Dict *dicts, Bytes *bts, Group *gr, Subband *sbs, md_t mode);
void ANS_dec_sb_reverse(Dict* dicts, Bytes *bts, Subband *sb, const ExtMode* md);


void createDirectTables(int **sizeses, int ***directTables, Dict *dicts, int nConts);
void createInverseTables(Dict *dicts, int nConts, int **sizeses, int ***directTables, InvTab **inverseTables);
void freeDirectTables(int nConts, Dict *dicts, int **sizeses, int ***directTables);
void freeInverseTables(int nConts, Dict *dicts, InvTab **inverseTables);

