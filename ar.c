#include "ar.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "constsAndTypes.h"


//////////// Ints ////////////

Ints emptyInts() {
	Ints x;
	x.len = 0;
	x.vals = NULL;
	return x;
}

Ints allocIntsLen(int len) {
	Ints x;
	x.len = len;
	x.vals = malloc(x.len * sizeof(int));
	memset(x.vals, 0, x.len * sizeof(int));
	return x;
}

void freeInts(Ints *x) {
	if (x->len != 0) {
		free(x->vals);
		x->len = 0;
	}
}

void changeIntsLen(Ints *x, int newLen) {
	x->len = newLen;
	x->vals = realloc(x->vals, x->len * sizeof(int));
}

Ints copyInts(const Ints* src) {
	Ints dst = allocIntsLen(src->len);
	memcpy(dst.vals, src->vals, src->len * sizeof(int));
	return dst;
}


//////////// Bytes ////////////

Bytes emptyBytes() {
	Bytes x;
	x.bytes = NULL;
	x.capacity = 0;
	x.posBit = BYTE_LEN - 1; ////////// 0
	x.posByte = 0;
	return x;
}

Bytes allocBytesLen(int len) {
	Bytes x;
	x.capacity = len;
	x.bytes = malloc(x.capacity * sizeof(uint8_t));
	memset(x.bytes, 0, x.capacity);
	x.posByte = 0;
	x.posBit = BYTE_LEN - 1; ////////// 0
	return x;
}

void freeBytes(Bytes *x) {
	if (x->capacity != 0) {
		free(x->bytes);
		x->capacity = 0;
		x->posByte = 0; 
		x->posBit = BYTE_LEN - 1; ////////// 0
	}
}

void reallocBytesLen(Bytes *x, int newLen) {
	x->bytes = realloc(x->bytes, newLen * sizeof(int));
	if (newLen > x->capacity) {
		memset(&(x->bytes[x->capacity]), 0, newLen - x->capacity);
	}
	x->capacity = newLen;
}

void rewindBytes(Bytes *x) {
	x->posByte = 0;
	x->posBit = BYTE_LEN - 1;
}

void addBitToBytes(Bytes *x, int bit) { //posBit: MSB - 7, LSB - 0
	bit &= 0x1;
	// x->bytes[x->posByte] |= (bit << (BYTE_LEN - 1 - x->posBit));
	x->bytes[x->posByte] |= (bit << (x->posBit));
	// if (++x->posBit == BYTE_LEN) {
	if (--x->posBit == -1) {
		// x->posBit = 0;
		x->posBit = BYTE_LEN - 1;
		if (++x->posByte == x->capacity) {
			reallocBytesLen(x, x->capacity + BYTES_ALLOC_STEP);
		}
	}
}
void addBitToBytes_reverse(Bytes *x, int bit) { 
	bit &= 0x1;
	// x->bytes[x->posByte] |= (bit << (x->posBit));
	x->bytes[x->posByte] |= (bit << (BYTE_LEN - 1 - x->posBit));
	// if (++x->posBit == BYTE_LEN) {
	if (--x->posBit == -1) {
		// x->posBit = 0;
		x->posBit = BYTE_LEN - 1;
		if (++x->posByte == x->capacity) {
			reallocBytesLen(x, x->capacity + BYTES_ALLOC_STEP);
		}
	}
}

void addNBitsToBytes(Bytes *x, int val, int nBits) { //or uint64_t val
	for (int i = nBits - 1; i >= 0; --i) {
		addBitToBytes(x, val >> i);
	}
}

void addNBitsToBytes_reverse(Bytes *x, int val, int nBits) { //or uint64_t val
	for (int i = nBits - 1; i >= 0; --i) {
		addBitToBytes_reverse(x, val >> i);
	}
}

int getRealBytesLen(Bytes b) {
	return (b.posBit == (BYTE_LEN - 1)) ? b.posByte : b.posByte + 1;
}

void alignBytes(Bytes *b) {
	if (b->posBit != (BYTE_LEN - 1)) {
		if (++b->posByte == b->capacity) {
			reallocBytesLen(b, b->capacity + BYTES_ALLOC_STEP);
		}
		b->posBit = BYTE_LEN - 1; ////////////0
	}
}

int writeBytesToFile(Bytes b, FILE *fp) {
	int nWrittenBytes = 0;
	int nBytes = getRealBytesLen(b);
	nWrittenBytes += fwrite(b.bytes, 1, nBytes, fp);
	return nWrittenBytes;
}

int deltaBits(Bytes big, Bytes small) {
	return (big.posByte - small.posByte) * BYTE_LEN + (-big.posBit + small.posBit);
}

int readBitFromBytes(Bytes *b) {
	int byte = b->bytes[b->posByte]; /////////////////////
	// int bit = (b->bytes[b->posByte] >> (BYTE_LEN - 1 - b->posBit)) & 0x1;
	int bit = (b->bytes[b->posByte] >> b->posBit) & 0x1;
	if (--b->posBit == -1) {
		++b->posByte;
		b->posBit = BYTE_LEN - 1;
	}
	return bit;
}

int readByteFromBytes(Bytes *bts) {
	if (bts->posBit != BYTE_LEN - 1) {
		printf("Bytes not aligned\n");
		exit(1);
	}
	if (bts->posByte == bts->capacity) {
		printf("The end of bytes has been reached\n");
		exit(1);
	}
	return bts->bytes[bts->posByte++];
}

int /*uint64_t*/ readNBitsFromBytes(Bytes *b, int nBits) {
	int res = 0;
	assert(nBits <= sizeof(res) * BYTE_LEN - 1);
	for (int i = 0; i < nBits; ++i) {
		res <<= 1;
		res |= readBitFromBytes(b);
	}
	return res;
}


void addByteToBytes(Bytes *b, uint8_t byte) {
	if (b->posBit == BYTE_LEN - 1) { //if aligned
		b->bytes[b->posByte] = byte;
		if (++b->posByte == b->capacity) {
			reallocBytesLen(b, b->capacity + BYTES_ALLOC_STEP);
		}
	}
	else {
		printf("Bytes are not aligned!\n"); //////////////////////
		b->bytes[b->posByte] |= (byte >> (BYTE_LEN - 1 - b->posBit));
		if (++b->posByte == b->capacity) {
			reallocBytesLen(b, b->capacity + BYTES_ALLOC_STEP);
		}
		b->bytes[b->posByte] = byte << (b->posBit + 1);
	}
}

// Bytes joinBytes(Bytes *a, Bytes *b) {
// 	Bytes res = allocBytesLen(getRealBytesLen(*a) + getRealBytesLen(*b));
// 	//slow. Use memcpy
// 	for (int i = 0; i < getRealBytesLen(*a); ++i) {
// 		addByteToBytes(&res, a->bytes[i]);
// 	}
// 	for (int i = 0; i < getRealBytesLen(*b); ++i) {
// 		addByteToBytes(&res, b->bytes[i]);
// 	}
// 	return res;
// }

void addBytesToBytes(Bytes *toBytes, Bytes *fromBytes) {
	for (int i = 0; i < getRealBytesLen(*fromBytes); ++i) {
		addByteToBytes(toBytes, fromBytes->bytes[i]);
	}
}

void addBytesToBytes_notAligned(Bytes *toBytes, Bytes *fromBytes) {
	for (int i = 0; i < getRealBytesLen(*fromBytes); ++i) {
		addByteToBytes(toBytes, fromBytes->bytes[i]);
	}
}

void readFileToBytes(FILE *fp, Bytes *bts) {
	fseek(fp, 0, SEEK_END);
	int fileLen = ftell(fp);

	*bts = allocBytesLen(fileLen);
	rewind(fp);
	int readSize = fread(bts->bytes, 1, fileLen, fp);
	
	assert(readSize == fileLen); //printf("readSize(%i) != fileLen(%i)\n", readSize, fileLen);
}




//////////// Bits ////////////

Bits emptyBits() {
	Bits x;
	x.data = 0;
	x.len = 0;
	return x;
}

void addBitToBits(Bits *buf, int bit) {
	buf->data <<= 1;
	buf->data |= (bit & 0x1);
	++buf->len;
}

void addNBitsToBits(Bits *buf, int val, int nBits) {
	uint32_t tmp = val;
	tmp <<= (sizeof(uint32_t) * BYTE_LEN - nBits);
	tmp >>= (sizeof(uint32_t) * BYTE_LEN - nBits);
	buf->data <<= nBits;
	buf->data |= (uint64_t)tmp;
	buf->len += nBits;
}

void addByteToBits(Bits *b, uint8_t byte) { //adding 8 bits to the right (least significant) of Bits b
	b->data <<= BYTE_LEN; //shifting all bits to the left
	b->data |= byte;	  //adding byte to the least significant position
	b->len += BYTE_LEN;	  //incrementing length
}

uint64_t readBits(Bits *b, int nBits) {
	if (nBits > (sizeof(b->data) * BYTE_LEN)) {
		printf("nBits > sizeof(b->data) * BYTE_LEN");
		exit(1);
	}
	if (nBits > b->len) {
		printf("nBits > b->len");
		exit(1);
	}

	int nRestBits = b->len - nBits;
	uint64_t res = (b->data >> nRestBits) & (~(~(uint64_t)0 << nBits)); //~(~0 << n) - nulls and n ones on the right (n least significant bits)
	b->len = nRestBits; //new len //b->len -= nBits; 

	return res;
}

int writeBitsToFile(Bits *b, FILE *fp) {
	int nWrittenBytes = 0;
	while (b->len > 0) {
		uint8_t byte = readBits(b, BYTE_LEN);
		nWrittenBytes += fwrite(&byte, 1, 1, fp);
	}
	return nWrittenBytes;
}

int readNBitsFromFile(Bits *b, FILE *fp, int nBits, uint64_t *res) {
	int nReadBytes = 0;
	while (b->len < nBits) {
		uint8_t byte;
		nReadBytes += fread(&byte, 1, 1, fp);
		addByteToBits(b, byte);
	}
	*res = readBits(b, nBits);
	return nReadBytes;
}

int readNBitsFromFileToInt(Bits *b, FILE *fp, int nBits, int *res) {
	int nReadBytes = 0;
	while (b->len < nBits) {
		uint8_t byte;
		nReadBytes += fread(&byte, 1, 1, fp);
		addByteToBits(b, byte);
	}
	*res = (int)readBits(b, nBits);
	return nReadBytes;
}

void addBitsToBytes(Bytes *x, Bits bits) {
	for (int i = bits.len - 1; i >= 0; --i) {
		addBitToBytes(x, bits.data >> i);
	}
}