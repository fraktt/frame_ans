#pragma once

#include <stdint.h>
#include <stdio.h>

typedef struct {
	int *vals;
	int len; //заменить на capacity
	//int pos;
	int capacity; //кол-во эл-ов, на которые выделена память
	int occupiedLen; //временное поле, позже заменить на len
} Ints; //array of ints

typedef struct {
	uint8_t *bytes;
	int capacity;
	int posByte;
	int posBit; //0 - MSB, 7 - LSB
} Bytes; //array of bytes

typedef struct {
	uint64_t data;
	int len; // data = 00 00 00 00 00 FF FF FF : len = 24
} Bits;

// Ints
Ints emptyInts();
Ints allocIntsLen(int len);
void freeInts(Ints *x);
void changeIntsLen(Ints *a, int len);
Ints copyInts(const Ints* src);

// Bytes
Bytes emptyBytes();
Bytes allocBytesLen(int len);
void freeBytes(Bytes *x);
void reallocBytesLen(Bytes *x, int newLen);
void rewindBytes(Bytes *x);
void addBitToBytes(Bytes *x, int bit);
void addBitToBytes_reverse(Bytes *x, int bit);
void addNBitsToBytes(Bytes *x, int val, int nBits);
void addNBitsToBytes_reverse(Bytes *x, int val, int nBits);
int getRealBytesLen(Bytes b);
void alignBytes(Bytes *b);
void addBitsToBytes(Bytes *x, Bits bits);
int writeBytesToFile(Bytes x, FILE *fp);
int deltaBits(Bytes a, Bytes b);
int readBitFromBytes(Bytes *b);
int readNBitsFromBytes(Bytes *b, int nBits);
void addByteToBytes(Bytes *b, uint8_t byte);
void addByteToBytes(Bytes *b, uint8_t byte);
void addBytesToBytes(Bytes *toBytes, Bytes *fromBytes);
void addBytesToBytes_notAligned(Bytes *toBytes, Bytes *fromBytes);
void readFileToBytes(FILE *fp, Bytes *bts);

// Bits
Bits emptyBits();
void addBitToBits(Bits *buf, int bit);
void addNBitsToBits(Bits *buf, int val, int nBits);
void addByteToBits(Bits *b, uint8_t byte);
uint64_t readBits(Bits *b, int nBits);
int writeBitsToFile(Bits *b, FILE *fp);
int readNBitsFromFile(Bits *b, FILE *fp, int nBits, uint64_t *res);
int readNBitsFromFileToInt(Bits *b, FILE *fp, int nBits, int *res);
void addBitsToBytes(Bytes *x, Bits bits);


