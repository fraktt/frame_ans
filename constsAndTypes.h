#pragma once

#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include "ar.h"

#define BYTE_LEN 8

//const unsigned char PRECISION = 4; //must not be less than 2
//#define PRECISION 12 //must not be less than 2
#define CONST_PROBABILITY_PRECISION 15 //15
#define MIN_PROBABILITY_PRECISION 10
#define MAX_PROBABILITY_PRECISION 20 //max compression - 17

#define N_SUBBANDS_1 8
#define N_SUBBANDS_2 9
#define N_SUBBANDS_ALL (N_SUBBANDS_1 + N_SUBBANDS_2)

// #define BYTES_ALLOC_STEP 1024
#define BYTES_ALLOC_STEP (1U << 26)

typedef enum { mode_coefs, mode_exps, mode_exps2, mode_exps3 } md_t;
typedef enum { dir_forw, dir_backw } dir_t;
typedef enum { alg_ans, alg_fse } alg_t;
typedef enum { sb_2, sb_1, sb_mixed } sb_t;
typedef enum { base_noCont, base_L, base_D, base_T, base_R, base_LD, base_DT, base_LT, base_LR, base_DR, base_LDT, base_LTR, base_LDTR,
				base_sumLT, base_sumLDTR, base_sumLL2, base_sumTT2, base_L2T2,
				base_sumLL2sumTT2, base_LL2TT2, base_TsumLL2, base_TLL2, base_LsumTT2, base_LTT2, base_LsumDT, base_LsumDTR, base_TsumLD,
				base_LTP } contBase_t;
typedef enum { z_nz, n_z_p, n_n1_z_p1_p, z_a1_a, ez_nz, ez2_nz, n_n12_z_p12_p, z_a12_a, n_ez_p, n_ez2_p } contType_t;
typedef enum { hf_plain, hf_norm } headerNorm_t; 
typedef enum { hs_noSort, hs_sortFreq, hs_sortAbs } headerSort_t;
typedef enum { prec_const, prec_adapt } prec_t;

typedef struct {
	contBase_t base;
	int typesAreDifferent; //1 - *types, 0 - uniformType
	contType_t *types;
	contType_t uniformType; 
	int isParentUsage; //1 - use parents, 0 - don't use
	contType_t pType;
} ContModel;

typedef struct {
	int **matrix;
	int nRows;
	int nColumns;
} Matrix;

typedef struct _Subband {
	int sbNum; //0 - 16
	sb_t sbType; //sb_2, sb_1 //(level of decomposition)
	int w;
	int h;
	int size; // = w * h
	Matrix matr;
	struct _Subband *parentPointer;

	ContModel cm; ////
	int nConts; ////
} Subband;

typedef struct {
	int groupNum;
	ContModel cm;
	Ints sbNums; //list of subband numbers that belong to this group ////
	sb_t sbsType; ////
	int nConts;
} Group;

typedef struct {
	Ints *lines;
	// Ints lines[N_SUBBANDS_ALL];
	int nLines; //num of elements in ar and lineSizes
	int total; // = sum of lineSizes
} FileContent;

typedef struct {
	int sym;
	int num;
	int freq;
} Sym;

typedef struct {
	Sym *syms;
	int len;
	int sumOfFreqs;
	//int prob_prec; //for normalized dicts
} Dict;

typedef struct {
	struct timespec time_enc_createDirectTable;
	struct timespec time_enc_proc;
	struct timespec time_enc_sepEsMants;
	struct timespec time_enc_makeNums; 
	struct timespec time_enc_renumSyms;
	struct timespec time_enc_normalizeDicts;
	struct timespec time_enc_writeHeader; 
	struct timespec time_encSb;
	struct timespec time_dec_createDirectTable;
	struct timespec time_dec_createInverseTable;
	struct timespec time_dec_proc;
	struct timespec time_decSb;
	struct timespec time_total;
} Times;


typedef struct {
	//int mode; //coefs, abs, mants&exps, ...
	md_t mode;
	dir_t dir;
	alg_t alg; //alg_ans, alg_fse
	headerNorm_t	headerNorm;
	headerSort_t	headerSort;
	contBase_t contBase;
	contType_t contType;
	prec_t precType; //
	int parentUsage; //1 - use parents, 0 - don't use 
	contType_t pType; 

	//int nConts; //
	int contModelsAreDifferent; //1 - *contMap, 0 - uniformContModel
	ContModel *contMap; //conts of subbands //contMap[N_SUBBANDS_ALL]
	ContModel uniformContModel; 

	int useGroups; //1 - use groups, 0 - don't use
	Ints *sbGroups; ////
	int nSbGroups; ////

	char *fileName;
	FILE *file;

	Times* times;
} ExtMode; //extended mode

typedef struct {
	int state;
	int symbol;
} InvTab;



