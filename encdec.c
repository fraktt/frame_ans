#include "encdec.h"

#include <math.h>
#include <assert.h>
// #include "time.h" ///////////////



void encGr(Group gr, Subband *sbs, Bytes *out, const ExtMode *md, Lengthes *len) {
	Ints nums		= emptyInts(); //input data
	Ints contFlags 	= emptyInts(); //dict number for each num
	int nConts 	= gr.nConts;
	Dict *dicts = allocEmptyDicts(nConts);

	Bytes mants = emptyBytes();
	if (md->mode == mode_exps || md->mode == mode_exps2) {
		sepEsMants(&gr, sbs, md->mode, &mants); //separate mants and signed exps
	}

	makeNums_gr(gr, sbs, &nums, &contFlags, dicts);
	renumberSyms(dicts, nConts, &nums, &contFlags, md, nConts);
	Dict *normDicts = makeNormalizedDicts(dicts, nConts);
	
	//for matlab
	// if (gr.groupNum == 0) { 
	// 	md->file = fopen(md->fileName, "w"); //create new empty file
	// } else {
	// 	md->file = fopen(md->fileName, "a"); //append to existing file
	// }
	// printDictsToFile_forMatlab(md->file, dicts, nConts);
	// fclose(md->file);

	int hBits_gr[4] = { 0 };
	int grSize = getGrSize(&gr, sbs); //for header
	alignBytes(out);
	int beforeHeader = getRealBytesLen(*out);
	writeHeader(out, md, grSize, dicts, normDicts, nConts, hBits_gr); //writing header
	len->header += getRealBytesLen(*out) - beforeHeader;
	len->hBits[0] = hBits_gr[0];
	len->hBits[1] = hBits_gr[1];
	len->hBits[2] = hBits_gr[2];
	len->hBits[3] = hBits_gr[3];
	
	alignBytes(out);
	int beforeEncData = getRealBytesLen(*out);
	ANS_enc(&nums, &contFlags, normDicts, out, md, nConts);
	len->enc += getRealBytesLen(*out) - beforeEncData;

	if (md->contBase == base_noCont) {
		len->encLim += (int)ceil(nums.len * entropy_dict(&(dicts[0]))); //shannon limit of encoded data		
	}

	if (md->mode == mode_exps || md->mode == mode_exps2) {
		alignBytes(out);
		addBytesToBytes(out, &mants);
		len->mants += getRealBytesLen(mants);
	}

	//free memory
	freeInts(&nums);
	freeInts(&contFlags);
	freeBytes(&mants);
	freeDictsAr(&dicts, nConts);
	freeDictsAr(&normDicts, nConts);
}

void encSb(Subband *sb, Bytes *out, const ExtMode* md, Lengthes *len) {
	Ints nums		= emptyInts(); //input data
	Ints contFlags 	= emptyInts(); //dict number for each num
	int nConts 	= sb->nConts;
	Dict *dicts = allocEmptyDicts(nConts);
	
	struct timespec start, end;

	Bytes mants = emptyBytes();
	if (md->mode != mode_coefs) {
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
		sepEsMants_sb(sb, md->mode, &mants); //separate mants and signed exps
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
		timespec_add(&md->times->time_enc_sepEsMants, timespec_sub(end, start));
	}

	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	makeNums_sb(sb, &nums, &contFlags, dicts);
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	timespec_add(&md->times->time_enc_makeNums, timespec_sub(end, start));

	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	renumberSyms(dicts, nConts, &nums, &contFlags, md, nConts); //перенумерация символов в dicts и nums
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	timespec_add(&md->times->time_enc_renumSyms, timespec_sub(end, start));

	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	Dict *normDicts = makeNormalizedDicts(dicts, nConts);
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	timespec_add(&md->times->time_enc_normalizeDicts, timespec_sub(end, start));

	//for matlab
	// if (gr.groupNum == 0) { 
	// 	md->file = fopen(md->fileName, "w"); //create new empty file
	// } else {
	// 	md->file = fopen(md->fileName, "a"); //append to existing file
	// }
	// printDictsToFile_forMatlab(md->file, dicts, nConts);
	// fclose(md->file);

	int hBits_sb[4] = { 0 };
	alignBytes(out);
	int beforeHeader = getRealBytesLen(*out);
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
	writeHeader(out, md, sb->size, dicts, normDicts, nConts, hBits_sb); //writing header
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
	timespec_add(&md->times->time_enc_writeHeader, timespec_sub(end, start));
	len->header += getRealBytesLen(*out) - beforeHeader;
	len->hBits[0] += hBits_sb[0]; 
	len->hBits[1] += hBits_sb[1]; 
	len->hBits[2] += hBits_sb[2]; 
	len->hBits[3] += hBits_sb[3]; 

	alignBytes(out); 
	int beforeEncData = getRealBytesLen(*out);
	ANS_enc(&nums, &contFlags, normDicts, out, md, nConts);
	len->enc += getRealBytesLen(*out) - beforeEncData;

	if (md->contBase == base_noCont) {
		len->encLim += (int)ceil(nums.len * entropy_dict(&(dicts[0]))); //shannon limit of encoded data		
	}

	if (md->mode != mode_coefs) {
		alignBytes(out); 
		addBytesToBytes(out, &mants);
		len->mants += getRealBytesLen(mants);
	}

	//free memory
	freeInts(&nums);
	freeInts(&contFlags);
	freeBytes(&mants);
	freeDictsAr(&dicts, nConts);
	freeDictsAr(&normDicts, nConts);
}

#include "fse.h"
void encSb_fse(Subband *sb, Bytes *out, const ExtMode* md, Lengthes *len) {
	Ints nums		= emptyInts(); //input data
	Ints contFlags 	= emptyInts(); //dict number for each num
	int nConts 	= sb->nConts;
	Dict *dicts = allocEmptyDicts(nConts);

	Bytes mants = emptyBytes();
	if (md->mode != mode_coefs) {
		sepEsMants_sb(sb, md->mode, &mants); //separate mants and signed exps
	}

	makeNums_sb(sb, &nums, &contFlags, dicts);
	renumberSyms(dicts, nConts, &nums, &contFlags, md, nConts); //renumbering
	
	int hBits_sb[4] = { 0 };
	alignBytes(out);
	assert(md->headerNorm == hf_plain); //записывать в хедер ненормализованные частоты
	writeHeader(out, md, sb->size, dicts, NULL, nConts, hBits_sb); //writing header
	len->header += getRealBytesLen(*out);
	len->hBits[0] += hBits_sb[0]; 
	len->hBits[1] += hBits_sb[1]; 
	len->hBits[2] += hBits_sb[2]; 
	len->hBits[3] += hBits_sb[3]; 


	/////////////////////////////////////////////////////// FSE
	Ints contNumses[nConts]; ////
	for (int cont = 0; cont < nConts; ++cont) {
		contNumses[cont] = allocIntsLen(nums.len); //чтобы точно хватило
	}
	int contNumsesSizes[nConts]; //occupied sizes of contNumses
	memset(contNumsesSizes, 0, sizeof(contNumsesSizes));
	for (int i = 0; i < nums.len; ++i) {
		int cont = contFlags.vals[i];
		contNumses[cont].vals[contNumsesSizes[cont]++] = nums.vals[i];
	}

	for (int cont = 0; cont < nConts; ++cont) {
		Ints *contNums = contNumses + cont;
		int srcSize = contNumsesSizes[cont];
		uint8_t src[srcSize]; //перевод входных символов из Ints в байты
		for (int i = 0; i < srcSize; ++i) {
			assert(contNums->vals[i] < 256);
			src[i] = contNums->vals[i];
		}
		alignBytes(out);
		size_t increm = FSE_compress(out->bytes + out->posByte, out->capacity - out->posByte, src, srcSize);
		out->posByte += increm;

		freeInts(contNumses + cont); 
	}
	///////////////////////////////////////////////////////
	

	len->enc += getRealBytesLen(*out) - len->header;

	if (md->contBase == base_noCont) {
		len->encLim += (int)ceil(nums.len * entropy_dict(&(dicts[0]))); //shannon limit of encoded data		
	}

	if (md->mode != mode_coefs) {
		alignBytes(out);
		addBytesToBytes(out, &mants);
		len->mants += getRealBytesLen(mants);
	}

	//free memory
	freeInts(&nums);
	freeInts(&contFlags);
	freeBytes(&mants);
	freeDictsAr(&dicts, nConts);
}



int getParentNum(int sbNum) {
	// return (sbNum - 8);
	switch (sbNum) { 
		case 9:		return 1;
		case 10:	return 2;
		case 11:	return 3;
		case 12:	return 4;
		case 13:	return 5;
		case 14:	return 6;
		case 15:	return 7;
		case 16:	return 8;
		default:	return -1;
	}
}

Subband* makeSubbands(const ExtMode* md, FileContent *fc, int sb2_w, int sb2_h, int sb1_w, int sb1_h) {
	Subband *sbs = malloc(N_SUBBANDS_ALL * sizeof(Subband));
	for (int i = 0; i < N_SUBBANDS_ALL; ++i) {
		Subband *sb = &(sbs[i]); //current subband
		*sb = emptySubband();
		sb->sbNum = i;
		sb->sbType = (sb->sbNum < N_SUBBANDS_2) ? sb_2 : sb_1;
		sb->w = (sb->sbType == sb_2) ? sb2_w : sb1_w;
		sb->h = (sb->sbType == sb_2) ? sb2_h : sb1_h;
		sb->size = sb->w * sb->h;
		sb->matr = createMatrixFromInts(&(fc->lines[sb->sbNum]), sb->w, sb->h);
		sb->parentPointer = (sb->sbType == sb_1) ? &(sbs[getParentNum(sb->sbNum)]) : NULL;

		sb->cm = md->contModelsAreDifferent ? md->contMap[sb->sbNum] : md->uniformContModel;
		sb->nConts = getNumOfConts(sb->cm); //here without considering parents
		assert(!((sb->sbType == sb_mixed) && (sb->cm.isParentUsage)));
		if (sb->sbType == sb_1 && sb->cm.isParentUsage) { //considering parents
			sb->nConts *= getNumOfContTypes(sb->cm.pType); 
		}
	}
	return sbs;
}

Subband* makeSubbands_notFilled(const ExtMode* md, int sb2_w, int sb2_h, int sb1_w, int sb1_h) {
	Subband *sbs = malloc(N_SUBBANDS_ALL * sizeof(Subband));
	for (int i = 0; i < N_SUBBANDS_ALL; ++i) {
		Subband *sb = &(sbs[i]); //current subband
		*sb = emptySubband();
		sb->sbNum = i;
		sb->sbType = (sb->sbNum < N_SUBBANDS_2) ? sb_2 : sb_1;
		sb->w = (sb->sbType == sb_2) ? sb2_w : sb1_w;
		sb->h = (sb->sbType == sb_2) ? sb2_h : sb1_h;
		sb->size = sb->w * sb->h;
		sb->matr = createMatrixWH(sb->w, sb->h);
		sb->parentPointer = (sb->sbType == sb_1) ? &(sbs[getParentNum(sb->sbNum)]) : NULL;

		sb->cm = md->contModelsAreDifferent ? md->contMap[sb->sbNum] : md->uniformContModel;
		sb->nConts = getNumOfConts(sb->cm); //here without considering parents
		if (sb->sbType == sb_1 && sb->cm.isParentUsage) { //considering parents
			sb->nConts *= getNumOfContTypes(sb->cm.pType); 
		}
	}
	return sbs;
}

Group* makeGroups(int nGroups, const ExtMode* md, Subband *sbs) {
	Group *grs = malloc(nGroups * sizeof(Group));
	for (int i = 0; i < nGroups; ++i) {
		Group *gr = &(grs[i]); //current group
		gr->groupNum = i;
		gr->sbNums = copyInts(&(md->sbGroups[i]));
		gr->sbsType = defineSbsType(sbs, &(gr->sbNums)); //type of group subbands
		gr->cm = md->contModelsAreDifferent ? md->contMap[i] : md->uniformContModel;
		assert(!((gr->sbsType == sb_mixed) && (gr->cm.isParentUsage))); //это требует переменного числа контекстов на протяжении группы
		gr->nConts = getNumOfConts(gr->cm); //here without considering parents
		if (gr->sbsType == sb_1 && gr->cm.isParentUsage) { //considering parents
			gr->nConts *= getNumOfContTypes(gr->cm.pType); 
		}
	}
	return grs;
}

void encFrame_gr(FileContent *ifc, Bytes *out, const ExtMode* md) {
	//getting sizes of subbands
	int sb2_w, sb2_h, sb1_w, sb1_h;
	if (getSubbandsWH(ifc->total, &sb2_w, &sb2_h, &sb1_w, &sb1_h)) {
		printf("wrong total size (%i)\n", ifc->total);
		exit(1);
	}

	//creating subbands and groups
	int nGroups = md->nSbGroups;
	Subband *sbs = makeSubbands(md, ifc, sb2_w, sb2_h, sb1_w, sb1_h);
	Group *grs = makeGroups(nGroups, md, sbs);

	//encoding groups
	Lengthes allLengthes = { 0 };
	*out = allocBytesLen(BYTES_ALLOC_STEP);
	for (int grNum = 0; grNum < nGroups; ++grNum) {
		Lengthes len_gr = { 0 };
		
		encGr(grs[grNum], sbs, out, md, &len_gr);
		
		printLengthes(len_gr, md, grNum, 'g');
		allLengthes = sumLengthes(allLengthes, len_gr); //incrementing allLengthes
	}
	printResultLengthes(allLengthes, md);
	
	//free
	free(grs);
	for (int i = 0; i < N_SUBBANDS_ALL; ++i) {
		freeSb(&(sbs[i]));
	}
	free(sbs);
}

void encFrame_sb(FileContent *ifc, Bytes *out, const ExtMode* md) {
	//getting sizes of subbands
	int sb2_w, sb2_h, sb1_w, sb1_h;
	if (getSubbandsWH(ifc->total, &sb2_w, &sb2_h, &sb1_w, &sb1_h)) {
		printf("wrong total size (%i)\n", ifc->total);
		exit(1);
	}

	//creating subbands
	Subband *sbs = makeSubbands(md, ifc, sb2_w, sb2_h, sb1_w, sb1_h);

	//encoding subbands
	Lengthes allLengthes = { 0 };
	*out = allocBytesLen(BYTES_ALLOC_STEP); 
	for (int sbNum = 0; sbNum < N_SUBBANDS_ALL; ++sbNum) {
		Lengthes len_sb = { 0 };
		
		struct timespec start, end;
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
		if (md->alg == alg_ans) {
			encSb(&(sbs[sbNum]), out, md, &len_sb);
		}
		else {
			encSb_fse(&(sbs[sbNum]), out, md, &len_sb);
		}
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
		timespec_add(&md->times->time_encSb, timespec_sub(end, start));
		
		printLengthes(len_sb, md, sbNum, 's');
		allLengthes = sumLengthes(allLengthes, len_sb); //incrementing allLengthes
	}
	
	printResultLengthes(allLengthes, md);
	
	//free
	for (int i = 0; i < N_SUBBANDS_ALL; ++i) {
		freeSb(&(sbs[i]));
	}
	free(sbs);
}

// void fromNumsToSyms(Dict dict, const Ints* nums, Ints *syms) {
// 	freeInts(syms);
// 	*syms = allocIntsLen(nums->len);
// 	for (int i = 0; i < nums->len; ++i) {
// 		syms->vals[i] = dict.syms[nums->vals[i]].sym;
// 	}
// }

void mergeEsMants_sb(Bytes *bts, Subband *sb) {
	//changing items of matrix: es -> (+mants) coefs
	for (int row = 0; row < sb->matr.nRows; ++row) {
		int *row_ptr = sb->matr.matrix[row];
		for (int col = 0; col < sb->matr.nColumns; ++col) {
			int *coef_ptr = row_ptr + col;
			
			int es = *coef_ptr;
			if (es >= -1 && es <= 1) {
				continue;
			}
			int sign = (es < 0);
			int e = sign ? -es : es; //abs(es)
			int mantBitLen = e - 1;
			int mant = readNBitsFromBytes(bts, mantBitLen);
			int absCoef = (1 << mantBitLen) | mant;
			*coef_ptr = sign ? -absCoef : absCoef; //write new coef <- es + mant
		}
	}
}

void mergeEsMants2_sb(Bytes *bts, Subband *sb) {
	//changing items of matrix: es -> (+mants) coefs
	for (int row = 0; row < sb->matr.nRows; ++row) {
		int *row_ptr = sb->matr.matrix[row];
		for (int col = 0; col < sb->matr.nColumns; ++col) {
			int *coef_ptr = row_ptr + col;

			int es2 = *coef_ptr;
			if (es2 >= -1 && es2 <= 1) {
				continue;
			}
			int sign = (es2 < 0);
			int e2 = sign ? -es2 : es2; //2,3,4,...
			e2 += 2; //4,5,6,...
			int mantFirstBit = e2 & 0x1;
			int e = e2 >> 1;

			int mantBitLen = e - 1;
			int mant2BitLen = mantBitLen - 1;
			int absCoef = (1 << mantBitLen) | (mantFirstBit << mant2BitLen);
			if (mant2BitLen > 0) {
				int restMant = readNBitsFromBytes(bts, mant2BitLen);
				absCoef |= restMant;
			}
			*coef_ptr = sign ? -absCoef : absCoef; //write new coef <- es + mant
		}
	}
}



void decGr(Bytes *bts, Group *gr, Subband *sbs, const ExtMode* md) {
	int nConts = gr->nConts;
	Dict *dicts = allocEmptyDicts(nConts);

	int grSize = getGrSize(gr, sbs);

	alignBytes(bts);
	readHeader(bts, grSize, dicts, nConts); //reading header (dicts)

	Dict *normDicts = makeNormalizedDicts_decEnv(dicts, nConts, md); //normalize dicts

	// // ANS_dec_gr(normDicts, encData, gr, sbs, md->mode); //decoding
	alignBytes(bts);
	ANS_dec_gr_reverse(normDicts, bts, gr, sbs, md->mode); //decoding

	if (md->mode == mode_exps || md->mode == mode_exps2) {
		alignBytes(bts);
		for (int sb_i = 0; sb_i < gr->sbNums.len; ++sb_i) {
			int sbNum = gr->sbNums.vals[sb_i];
			Subband *sb = &(sbs[sbNum]);
			if (md->mode == mode_exps) {
				mergeEsMants_sb(bts, sb);
			}
			else /*if (md->mode == mode_exps2)*/ {
				mergeEsMants2_sb(bts, sb);
			}
			
		}
	}

	//freeBytes(&encData);
	freeDictsAr(&dicts, nConts);
	freeDictsAr(&normDicts, nConts);
}



void decSb(Bytes *bts, Subband *sb, const ExtMode* md) {
	int nConts = sb->nConts;
	Dict *dicts = allocEmptyDicts(nConts);

	alignBytes(bts);
	readHeader(bts, sb->size, dicts, nConts); //reading header (dicts)

	Dict *normDicts = makeNormalizedDicts_decEnv(dicts, nConts, md); //normalize dicts

	alignBytes(bts);
	ANS_dec_sb_reverse(normDicts, bts, sb, md); //decoding
	
	if (md->mode == mode_exps || md->mode == mode_exps2) {
		alignBytes(bts);
		if (md->mode == mode_exps) {
			mergeEsMants_sb(bts, sb);
		}
		else /*if (md->mode == mode_exps2)*/ {
			mergeEsMants2_sb(bts, sb);
		}
	}

	freeDictsAr(&dicts, nConts);
	freeDictsAr(&normDicts, nConts);
}




void decFrame_gr(Bytes *in, int total, FileContent *ofc, const ExtMode* md) {
	//getting sizes
	int sb2_w, sb2_h, sb1_w, sb1_h;
	if (getSubbandsWH(total, &sb2_w, &sb2_h, &sb1_w, &sb1_h)) {
		printf("wrong total size (%i)\n", total);
		exit(1);
	}

	//creating groups structures
	int nGroups = md->nSbGroups;
	Subband *sbs = makeSubbands_notFilled(md, sb2_w, sb2_h, sb1_w, sb1_h);
	Group *grs = makeGroups(nGroups, md, sbs);

	//decoding groups
	for (int i = 0; i < nGroups; ++i) {
		decGr(in, &(grs[i]), sbs, md);
	}

	//writing coefs to FileContent struct
	ofc->total = total;
	ofc->nLines = N_SUBBANDS_ALL;
	ofc->lines = malloc(ofc->nLines * sizeof(Ints));
	for (int i = 0; i < N_SUBBANDS_ALL; ++i) {
		Subband *sb = &(sbs[i]);
		ofc->lines[sb->sbNum] = allocIntsLen(sb->size);

		int cnt = 0;
		for (int row = 0; row < sb->matr.nRows; ++row) {
			int *row_ptr = sb->matr.matrix[row];
			for (int col = 0; col < sb->matr.nColumns; ++col) {
				ofc->lines[sb->sbNum].vals[cnt++] = row_ptr[col];
			}
		}
	}

	//free memory
	free(grs);
	for (int i = 0; i < N_SUBBANDS_ALL; ++i) {
		freeSb(&(sbs[i]));
	}
	free(sbs);
}



void decFrame_sb(Bytes *in, int total, FileContent *ofc, const ExtMode* md) {
	//getting sizes
	int sb2_w, sb2_h, sb1_w, sb1_h;
	if (getSubbandsWH(total, &sb2_w, &sb2_h, &sb1_w, &sb1_h)) {
		printf("wrong total size (%i)\n", total);
		exit(1);
	}

	//creating subbands structures
	Subband *sbs = makeSubbands_notFilled(md, sb2_w, sb2_h, sb1_w, sb1_h);

	//decoding subbands
	for (int i = 0; i < N_SUBBANDS_ALL; ++i) {
		struct timespec start, end;
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);
		decSb(in, &(sbs[i]), md);
		clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);
		timespec_add(&md->times->time_decSb, timespec_sub(end, start));
	}

	//writing coefs to FileContent struct
	ofc->total = total;
	ofc->nLines = N_SUBBANDS_ALL;
	ofc->lines = malloc(ofc->nLines * sizeof(Ints));
	for (int i = 0; i < N_SUBBANDS_ALL; ++i) {
		Subband *sb = &(sbs[i]);
		ofc->lines[sb->sbNum] = allocIntsLen(sb->size);

		int cnt = 0;
		for (int row = 0; row < sb->matr.nRows; ++row) {
			int *row_ptr = sb->matr.matrix[row];
			for (int col = 0; col < sb->matr.nColumns; ++col) {
				ofc->lines[sb->sbNum].vals[cnt++] = row_ptr[col];
			}
		}
	}

	//free memory
	for (int i = 0; i < N_SUBBANDS_ALL; ++i) {
		freeSb(&(sbs[i]));
	}
	free(sbs);
}



