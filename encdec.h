#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>

#include "prints.h"
#include "constsAndTypes.h"
#include "ans.h"
#include "lengthes.h"
#include "func.h"

void encFrame_gr(FileContent *ifc, Bytes *out, const ExtMode* md);
void encFrame_sb(FileContent *ifc, Bytes *out, const ExtMode* md);
void decFrame_gr(Bytes *in, int total, FileContent *ofc, const ExtMode* md);
void decFrame_sb(Bytes *in, int total, FileContent *ofc, const ExtMode* md);

void encSb(Subband *sb, Bytes *out, const ExtMode* md, Lengthes *len);