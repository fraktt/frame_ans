#pragma once

#include <stdlib.h>



size_t FSE_compress (void* dst, size_t dstCapacity, const void* src, size_t srcSize);