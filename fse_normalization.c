# include <stdint.h>
typedef uint8_t  BYTE;
typedef uint16_t U16;
typedef uint32_t U32;
typedef  int32_t S32;
typedef uint64_t U64;

#include "fse_normalization.h"

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>


typedef enum {
  FSE_error_no_error,
  FSE_error_GENERIC,
  FSE_error_dstSize_tooSmall,
  FSE_error_srcSize_wrong,
  FSE_error_corruption_detected,
  FSE_error_tableLog_tooLarge,
  FSE_error_maxSymbolValue_tooLarge,
  FSE_error_maxSymbolValue_tooSmall,
  FSE_error_workSpace_tooSmall,
  FSE_error_maxCode
} FSE_ErrorCode;


#define PREFIX(name) FSE_error_##name
#define ERROR(name) ((size_t)-PREFIX(name))


/* **************************************************************
*  Tuning parameters
****************************************************************/
/*!MEMORY_USAGE :
*  Memory usage formula : N->2^N Bytes (examples : 10 -> 1KB; 12 -> 4KB ; 16 -> 64KB; 20 -> 1MB; etc.)
*  Increasing memory usage improves compression ratio
*  Reduced memory usage can improve speed, due to cache effect
*  Recommended max value is 14, for 16KB, which nicely fits into Intel x86 L1 cache */
#ifndef FSE_MAX_MEMORY_USAGE
#  define FSE_MAX_MEMORY_USAGE 14
#endif
#ifndef FSE_DEFAULT_MEMORY_USAGE
#  define FSE_DEFAULT_MEMORY_USAGE 13
#endif


#define FSE_MAX_TABLELOG  (FSE_MAX_MEMORY_USAGE-2)
#define FSE_MAX_TABLESIZE (1U<<FSE_MAX_TABLELOG)
#define FSE_MAXTABLESIZE_MASK (FSE_MAX_TABLESIZE-1)
#define FSE_DEFAULT_TABLELOG (FSE_DEFAULT_MEMORY_USAGE-2)
#define FSE_MIN_TABLELOG 5



#define FSE_isError ERR_isError

#  define ERR_STATIC static __attribute__((unused))
ERR_STATIC unsigned ERR_isError(size_t code) { return (code > ERROR(maxCode)); }

#  define MEM_STATIC static __inline __attribute__((unused))


MEM_STATIC unsigned BIT_highbit32 (U32 val)
{
    assert(val != 0);
    {
#   if defined(_MSC_VER)   /* Visual */
        unsigned long r=0;
        _BitScanReverse ( &r, val );
        return (unsigned) r;
#   elif defined(__GNUC__) && (__GNUC__ >= 3)   /* Use GCC Intrinsic */
        return 31 - __builtin_clz (val);
#   else   /* Software version */
        static const unsigned DeBruijnClz[32] = { 0,  9,  1, 10, 13, 21,  2, 29,
                                                 11, 14, 16, 18, 22, 25,  3, 30,
                                                  8, 12, 20, 28, 15, 17, 24,  7,
                                                 19, 27, 23,  6, 26,  5,  4, 31 };
        U32 v = val;
        v |= v >> 1;
        v |= v >> 2;
        v |= v >> 4;
        v |= v >> 8;
        v |= v >> 16;
        return DeBruijnClz[ (U32) (v * 0x07C4ACDDU) >> 27];
#   endif
    }
}



/* provides the minimum logSize to safely represent a distribution */
static unsigned FSE_minTableLog(size_t srcSize, unsigned maxSymbolValue)
{
    U32 minBitsSrc = BIT_highbit32((U32)(srcSize - 1)) + 1;
    U32 minBitsSymbols = BIT_highbit32(maxSymbolValue) + 2;
    U32 minBits = minBitsSrc < minBitsSymbols ? minBitsSrc : minBitsSymbols;
    assert(srcSize > 1); /* Not supported, RLE should be used instead */
    return minBits;
}



static size_t FSE_normalizeM2(short* norm, U32 tableLog, const unsigned* count, size_t total, U32 maxSymbolValue)
{
    short const NOT_YET_ASSIGNED = -2;
    U32 s;
    U32 distributed = 0;
    U32 ToDistribute;

    /* Init */
    U32 const lowThreshold = (U32)(total >> tableLog);
    U32 lowOne = (U32)((total * 3) >> (tableLog + 1));

    for (s=0; s<=maxSymbolValue; s++) {
        if (count[s] == 0) {
            norm[s]=0;
            continue;
        }
        if (count[s] <= lowThreshold) {
            norm[s] = -1;
            distributed++;
            total -= count[s];
            continue;
        }
        if (count[s] <= lowOne) {
            norm[s] = 1;
            distributed++;
            total -= count[s];
            continue;
        }

        norm[s]=NOT_YET_ASSIGNED;
    }
    ToDistribute = (1 << tableLog) - distributed;

    if ((total / ToDistribute) > lowOne) {
        /* risk of rounding to zero */
        lowOne = (U32)((total * 3) / (ToDistribute * 2));
        for (s=0; s<=maxSymbolValue; s++) {
            if ((norm[s] == NOT_YET_ASSIGNED) && (count[s] <= lowOne)) {
                norm[s] = 1;
                distributed++;
                total -= count[s];
                continue;
        }   }
        ToDistribute = (1 << tableLog) - distributed;
    }

    if (distributed == maxSymbolValue+1) {
        /* all values are pretty poor;
           probably incompressible data (should have already been detected);
           find max, then give all remaining points to max */
        U32 maxV = 0, maxC = 0;
        for (s=0; s<=maxSymbolValue; s++)
            if (count[s] > maxC) { maxV=s; maxC=count[s]; }
        norm[maxV] += (short)ToDistribute;
        return 0;
    }

    if (total == 0) {
        /* all of the symbols were low enough for the lowOne or lowThreshold */
        for (s=0; ToDistribute > 0; s = (s+1)%(maxSymbolValue+1))
            if (norm[s] > 0) { ToDistribute--; norm[s]++; }
        return 0;
    }

    {   U64 const vStepLog = 62 - tableLog;
        U64 const mid = (1ULL << (vStepLog-1)) - 1;
        U64 const rStep = ((((U64)1<<vStepLog) * ToDistribute) + mid) / total;   /* scale on remaining */
        U64 tmpTotal = mid;
        for (s=0; s<=maxSymbolValue; s++) {
            if (norm[s]==NOT_YET_ASSIGNED) {
                U64 const end = tmpTotal + (count[s] * rStep);
                U32 const sStart = (U32)(tmpTotal >> vStepLog);
                U32 const sEnd = (U32)(end >> vStepLog);
                U32 const weight = sEnd - sStart;
                if (weight < 1)
                    return ERROR(GENERIC);
                norm[s] = (short)weight;
                tmpTotal = end;
    }   }   }

    return 0;
}




//по сравнению с оригинальной ф-ией:
//отключена проверка FSE_minTableLog
//отключено присваивание -1 частоты
size_t FSE_normalizeCount (short* normalizedCounter, unsigned tableLog,
                           const unsigned* count, size_t total,
                           unsigned maxSymbolValue)
{
    /* Sanity checks */
    if (tableLog==0) tableLog = FSE_DEFAULT_TABLELOG;
    if (tableLog < FSE_MIN_TABLELOG) return ERROR(GENERIC);   /* Unsupported size */
    // if (tableLog > FSE_MAX_TABLELOG) return ERROR(tableLog_tooLarge);   /* Unsupported size */
    // if (tableLog < FSE_minTableLog(total, maxSymbolValue)) return ERROR(GENERIC);   /* Too small tableLog, compression potentially impossible */

    {   static U32 const rtbTable[] = {     0, 473195, 504333, 520860, 550000, 700000, 750000, 830000 };
        U64 const scale = 62 - tableLog;
        U64 const step = ((U64)1<<62) / total;   /* <== here, one division ! */
        U64 const vStep = 1ULL<<(scale-20);
        int stillToDistribute = 1<<tableLog;
        unsigned s;
        unsigned largest=0;
        short largestP=0;
        U32 lowThreshold = (U32)(total >> tableLog);

        for (s=0; s<=maxSymbolValue; s++) {
            // if (count[s] == total) return 0;   /* rle special case */ ////////////////////////////
            if (count[s] == 0) { normalizedCounter[s]=0; continue; }
            if (count[s] <= lowThreshold) {
                // normalizedCounter[s] = -1;
                normalizedCounter[s] = 1; //////////////////////////////
                stillToDistribute--;
            } else {
                short proba = (short)((count[s]*step) >> scale);
                if (proba < 0) { ///////////////////////////////
                    printf("%u %lu %lu\n", count[s], step, scale);
                    assert(proba >= 0);
                }
                if (proba<8) {
                    U64 restToBeat = vStep * rtbTable[proba];
                    proba += (count[s]*step) - ((U64)proba<<scale) > restToBeat;
                }
                if (proba > largestP) { largestP=proba; largest=s; }
                normalizedCounter[s] = proba;
                stillToDistribute -= proba;
            }
        }   
        if (-stillToDistribute >= (normalizedCounter[largest] >> 1)) {
            /* corner case, need another normalization method */
            size_t const errorCode = FSE_normalizeM2(normalizedCounter, tableLog, count, total, maxSymbolValue);
            if (FSE_isError(errorCode)) return errorCode;
        }
        else normalizedCounter[largest] += (short)stillToDistribute;        
    }

#if 0
    {   /* Print Table (debug) */
        U32 s;
        U32 nTotal = 0;
        for (s=0; s<=maxSymbolValue; s++)
            RAWLOG(2, "%3i: %4i \n", s, normalizedCounter[s]);
        for (s=0; s<=maxSymbolValue; s++)
            nTotal += abs(normalizedCounter[s]);
        if (nTotal != (1U<<tableLog))
            RAWLOG(2, "Warning !!! Total == %u != %u !!!", nTotal, 1U<<tableLog);
        getchar();
    }
#endif

    return tableLog;
}









#define FSE_FUNCTION_TYPE BYTE
#define FSE_MAX_SYMBOL_VALUE 255
//in fseU16.c:
// #define FSEU16_MAX_SYMBOL_VALUE 286   /* This is just an example, typical value for zlib */
// #define FSE_MAX_SYMBOL_VALUE FSEU16_MAX_SYMBOL_VALUE
// #define FSE_FUNCTION_TYPE U16 

typedef unsigned FSE_CTable;   /* don't allocate that. It's only meant to be more restrictive than void* */

#define FSE_TABLESTEP(tableSize) ((tableSize>>1) + (tableSize>>3) + 3)

/* FSE_buildCTable_wksp() :
 * Same as FSE_buildCTable(), but using an externally allocated scratch buffer (`workSpace`).
 * wkspSize should be sized to handle worst case situation, which is `1<<max_tableLog * sizeof(FSE_FUNCTION_TYPE)`
 * workSpace must also be properly aligned with FSE_FUNCTION_TYPE requirements
 */
// size_t FSE_buildCTable_wksp(/*FSE_CTable* ct,*/ const short* normalizedCounter, unsigned maxSymbolValue, unsigned tableLog, void* workSpace, size_t wkspSize) {
size_t fseSpread(const short* normalizedCounter, unsigned maxSymbolValue, unsigned tableLog, int* tableSymbol, size_t wkspSize) {
    U32 const tableSize = 1 << tableLog;
    U32 const tableMask = tableSize - 1;
    // void* const ptr = ct;
    // U16* const tableU16 = ( (U16*) ptr) + 2;
    // void* const FSCT = ((U32*)ptr) + 1 /* header */ + (tableLog ? tableSize>>1 : 1) ;
    // FSE_symbolCompressionTransform* const symbolTT = (FSE_symbolCompressionTransform*) (FSCT);
    U32 const step = FSE_TABLESTEP(tableSize);
    // U32 cumul[FSE_MAX_SYMBOL_VALUE+2];

    // FSE_FUNCTION_TYPE* const tableSymbol = (FSE_FUNCTION_TYPE*)workSpace;
    U32 highThreshold = tableSize-1;

    /* CTable header */
    if (((size_t)1 << tableLog) * sizeof(FSE_FUNCTION_TYPE) > wkspSize) {
		assert(0); ////////////////////////////
		return ERROR(tableLog_tooLarge);
	}
    // tableU16[-2] = (U16) tableLog;
    // tableU16[-1] = (U16) maxSymbolValue;
    assert(tableLog < 16);   /* required for the threshold strategy to work */

    /* For explanations on how to distribute symbol values over the table :
    *  http://fastcompression.blogspot.fr/2014/02/fse-distributing-symbol-values.html */

    /* symbol start positions */
    // {   U32 u;
    //     cumul[0] = 0;
    //     for (u=1; u<=maxSymbolValue+1; u++) {
    //         if (normalizedCounter[u-1]==-1) {  /* Low proba symbol */
    //             cumul[u] = cumul[u-1] + 1;
    //             tableSymbol[highThreshold--] = (FSE_FUNCTION_TYPE)(u-1);
    //         } else {
    //             cumul[u] = cumul[u-1] + normalizedCounter[u-1];
    //     }   }
    //     cumul[maxSymbolValue+1] = tableSize+1;
    // }

    /* Spread symbols */
    {   
		U32 position = 0;
        for (int symbol = 0; symbol <= maxSymbolValue; symbol++) {
			printf("%i: i, nc: %i\n", symbol, normalizedCounter[symbol]);
            for (int nbOccurences = 0; nbOccurences < normalizedCounter[symbol]; nbOccurences++) {
                tableSymbol[position] = symbol;
                position = (position + step) & tableMask;
                while (position > highThreshold) position = (position + step) & tableMask;   /* Low proba area */
        	}   
		}

        if (position!=0) return ERROR(GENERIC);   /* Must have gone through all positions */
    }
  
    return 0;
}


// #define FSE_CTABLE_SIZE_U32(maxTableLog, maxSymbolValue)   (1 + (1<<(maxTableLog-1)) + ((maxSymbolValue+1)*2))

// /* FSE_compress_wksp() :
//  * Same as FSE_compress2(), but using an externally allocated scratch buffer (`workSpace`).
//  * `wkspSize` size must be `(1<<tableLog)`.
//  */
// size_t FSE_compress_wksp (void* dst, size_t dstSize, const void* src, size_t srcSize, unsigned maxSymbolValue, unsigned tableLog, void* workSpace, size_t wkspSize)
// {
//     BYTE* const ostart = (BYTE*) dst;
//     BYTE* op = ostart;
//     BYTE* const oend = ostart + dstSize;

//     U32   count[FSE_MAX_SYMBOL_VALUE+1];
//     S16   norm[FSE_MAX_SYMBOL_VALUE+1];
//     FSE_CTable* CTable = (FSE_CTable*)workSpace;
//     size_t const CTableSize = FSE_CTABLE_SIZE_U32(tableLog, maxSymbolValue);
//     void* scratchBuffer = (void*)(CTable + CTableSize);
//     size_t const scratchBufferSize = wkspSize - (CTableSize * sizeof(FSE_CTable));

//     /* init conditions */
//     if (wkspSize < FSE_WKSP_SIZE_U32(tableLog, maxSymbolValue)) return ERROR(tableLog_tooLarge);
//     if (srcSize <= 1) return 0;  /* Not compressible */
//     if (!maxSymbolValue) maxSymbolValue = FSE_MAX_SYMBOL_VALUE;
//     if (!tableLog) tableLog = FSE_DEFAULT_TABLELOG;

//     /* Scan input and build symbol stats */
//     {   CHECK_V_F(maxCount, HIST_count_wksp(count, &maxSymbolValue, src, srcSize, (unsigned*)scratchBuffer) );
//         if (maxCount == srcSize) return 1;   /* only a single symbol in src : rle */
//         if (maxCount == 1) return 0;         /* each symbol present maximum once => not compressible */
//         if (maxCount < (srcSize >> 7)) return 0;   /* Heuristic : not compressible enough */
//     }

//     tableLog = FSE_optimalTableLog(tableLog, srcSize, maxSymbolValue);
//     CHECK_F( FSE_normalizeCount(norm, tableLog, count, srcSize, maxSymbolValue) );

//     /* Write table description header */
//     {   CHECK_V_F(nc_err, FSE_writeNCount(op, oend-op, norm, maxSymbolValue, tableLog) );
//         op += nc_err;
//     }

//     /* Compress */
//     CHECK_F( FSE_buildCTable_wksp(CTable, norm, maxSymbolValue, tableLog, scratchBuffer, scratchBufferSize) );
//     {   CHECK_V_F(cSize, FSE_compress_usingCTable(op, oend - op, src, srcSize, CTable) );
//         if (cSize == 0) return 0;   /* not enough space for compressed data */
//         op += cSize;
//     }

//     /* check compressibility */
//     if ( (size_t)(op-ostart) >= srcSize-1 ) return 0;

//     return op-ostart;
// }






// typedef struct {
//     FSE_CTable CTable_max[FSE_CTABLE_SIZE_U32(FSE_MAX_TABLELOG, FSE_MAX_SYMBOL_VALUE)];
//     BYTE scratchBuffer[1 << FSE_MAX_TABLELOG];
// } fseWkspMax_t;

// size_t FSE_compress2 (void* dst, size_t dstCapacity, const void* src, size_t srcSize, unsigned maxSymbolValue, unsigned tableLog)
// {
//     fseWkspMax_t scratchBuffer;
//     DEBUG_STATIC_ASSERT(sizeof(scratchBuffer) >= FSE_WKSP_SIZE_U32(FSE_MAX_TABLELOG, FSE_MAX_SYMBOL_VALUE));   /* compilation failures here means scratchBuffer is not large enough */
//     if (tableLog > FSE_MAX_TABLELOG) return ERROR(tableLog_tooLarge);
//     return FSE_compress_wksp(dst, dstCapacity, src, srcSize, maxSymbolValue, tableLog, &scratchBuffer, sizeof(scratchBuffer));
// }


// size_t FSE_compress (void* dst, size_t dstCapacity, const void* src, size_t srcSize)
// {
//     return FSE_compress2(dst, dstCapacity, src, srcSize, FSE_MAX_SYMBOL_VALUE, FSE_DEFAULT_TABLELOG);
// }