#pragma once

#include <stddef.h>        /* size_t */

size_t FSE_normalizeCount (short* normalizedCounter, unsigned tableLog,
                           const unsigned* count, size_t total,
                           unsigned maxSymbolValue);

size_t fseSpread(const short* normalizedCounter, unsigned maxSymbolValue, unsigned tableLog, int* tableSymbol, size_t wkspSize);