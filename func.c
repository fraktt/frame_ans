#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "func.h"
#include "ar.h"
// #include "fse_normalization.h" ///////////////////

int probPrec() {
	int PROBABILITY_PRECISION = /*bitlen(nSymbols) + PRECISION*/ CONST_PROBABILITY_PRECISION;
	return PROBABILITY_PRECISION;
}
//int probPrec_2(ExtMode md, Dict dict) {
//	int PROBABILITY_PRECISION = 0;
//	if (md.precType == prec_const) {
//		PROBABILITY_PRECISION = /*bitlen(nSymbols) + PRECISION*/ CONST_PROBABILITY_PRECISION;
//	}
//	else if (md.precType == prec_adapt) {
//		PROBABILITY_PRECISION = (int)(round(log(dict.sumOfFreqs) / log(2)) - 1);
//		if (PROBABILITY_PRECISION < MIN_PROBABILITY_PRECISION) {
//			PROBABILITY_PRECISION = MIN_PROBABILITY_PRECISION;
//		}
//		if (PROBABILITY_PRECISION > MAX_PROBABILITY_PRECISION) {
//			PROBABILITY_PRECISION = MAX_PROBABILITY_PRECISION;
//		}
//	} 
//	return PROBABILITY_PRECISION;
//}



int readFileContent(FILE *fp, FileContent *fc) {
	//counting number of coefficients in file
	rewind(fp);
	fc->total = 0;
	while (1) {
		int tmp = 0;
		int res = fscanf(fp, "%i", &tmp);
		if (res != 1) {
			break;
		}
		++fc->total;
	}

	//calculating sizes of subbands
	int subband2_size, subband1_size;
	if (getSubbandsSizes(fc->total, &subband2_size, &subband1_size)) {
		printf("wrong total size (%i)\n", fc->total);
		return -1;
	}
	printf("Total size = %i; subband2_size = %i; subband1_size = %i\n", fc->total, subband2_size, subband1_size);

	//allocating memory for lines
	fc->nLines = N_SUBBANDS_ALL;
	fc->lines = malloc(fc->nLines * sizeof(Ints));
	
	//reading subbands
	rewind(fp);
	for (int line = 0; line < fc->nLines; ++line) {
		fc->lines[line] = allocIntsLen((line < N_SUBBANDS_2) ? subband2_size : subband1_size);
		for (int j = 0; j < fc->lines[line].len; j++) { //filling line with coefs
			if (fscanf(fp, "%i", &(fc->lines[line].vals[j])) != 1) {
				printf("error reading subband[%i]\n", line);
				return -1;
			}
		}
	}
	return 0;
}

//DANGER! (not working)
//int readFileContent2(FILE *fp, FileContent *fc) {
//	rewind(fp);
//	//char str[2000000] = { 0 }; //230400 * ((5+1)+1) = 1612800
//	char *str = malloc(2000000);/////////
//	memset(str, 0, 2000000);///////////
//	fc->nLines = 0;
//	fc->lineSizes = NULL;
//	fc->ar = NULL;
//
//	while (1) {
//		//reading string
//		fgets(str, 2000000, fp);
//		if (str == NULL) {
//			break;
//		}
//		++fc->nLines;
//		
//		//splitting to tokens
//		int *ar = malloc(strlen(str) * sizeof(int));
//		int arLen = 0;
//		char *token, *nextToken;
//		token = strtok_s(str, " ", &nextToken);
//		while (token != NULL) {
//			ar[arLen++] = atoi(token);
//			token = strtok_s(NULL, " ", &nextToken);
//		}
//
//		//adding new item to fc->lineSizes
//		if (fc->lineSizes == NULL) {
//			fc->lineSizes = malloc(fc->nLines * sizeof(int));
//		}
//		else {
//			fc->lineSizes = realloc(fc->lineSizes, fc->nLines * sizeof(int));
//		}
//		fc->lineSizes[fc->nLines - 1] = arLen;
//		fc->total += fc->lineSizes[fc->nLines - 1];
//
//		//adding new item to fc->ar
//		if (fc->ar = NULL) {
//			fc->ar = malloc(fc->nLines * sizeof(int16_t*));
//		}
//		else {
//			fc->ar = realloc(fc->ar, fc->nLines * sizeof(int16_t*));
//		}
//		fc->ar[fc->nLines - 1] = malloc(fc->lineSizes[fc->nLines - 1] * sizeof(int16_t));
//		for (int i = 0; i < arLen; ++i) {
//			fc->ar[fc->nLines - 1][i] = (int16_t)ar[i];
//		}
//
//		free(ar);
//	}
//
//	free(str);///////////
//}

//int readFileContent3(FILE *fp, FileContent *fc) {
//	char buf[64] = { 0 };
//	int bufLen = 0;
//	rewind(fp);
//	fc->total = 0;
//	while (1) {
//		char tmp = 0;
//		int res = fscanf(fp, "%c", &tmp);
//		if (res != 1) { //EOF
//			break;
//		}
//		else if (tmp == '\n') {
//			//new row
//		}
//		else if (tmp == ' ') {
//			int x = atoi(buf);
//
//			++fc->total;
//		}
//		else {
//			buf[bufLen++] = tmp;
//		}
//	}
//
//	
//}


double entropy(int *freqs, int freqsLen) {

	int len = 0;
	for (int i = 0; i < freqsLen; ++i) {
		len += freqs[i];
	}

	//entropy calculation
	double H = 0;
	for (int i = 0; i < freqsLen; ++i) {
		if (freqs[i] != 0) {
			double p = (double)freqs[i] / len;
			H -= p * log(p) / log(256);
		}
	}

	return H;
}


int checkFilesEquality(char *file1, char *file2) { ////////////////////////////////
	FILE *fp1, *fp2;
	fp1 = fopen(file1, "rb");
	fp2 = fopen(file2, "rb");

	int byteNum = 1;
	const int BLOCK = 4096;
	uint8_t buff1[4096], buff2[4096];
	int readed1, readed2;
	while (1) {
		readed1 = fread(buff1, 1, BLOCK, fp1);
		readed2 = fread(buff2, 1, BLOCK, fp2);

		if (readed1 != readed2) {
			int i; //used in return
			for (i = 0; i < ((readed1 < readed2) ? readed1 : readed2); ++i) {
				if (buff1[i] != buff2[i]) {
					fclose(fp1);
					fclose(fp2);
					return (byteNum + i);
				}
			}
			fclose(fp1);
			fclose(fp2);
			return (byteNum + i + 1);
		}

		if (readed1 == 0) { //readed1 == readed2 == 0
			break;
		}

		for (int i = 0; i < readed1; ++i) { //readed1 == readed2 != 0
			if (buff1[i] != buff2[i]) {
				fclose(fp1);
				fclose(fp2);
				return (byteNum + i);
			}
		}

		byteNum += readed1;
	}

	fclose(fp1);
	fclose(fp2);
	return 0;
}


int compareCoefFiles(FILE *fp1, FILE *fp2) {
	rewind(fp1);
	rewind(fp2);
	int length = 0;
	while (1) {
		int x1, x2;
		int res1 = fscanf(fp1, "%i", &x1);
		int res2 = fscanf(fp2, "%i", &x2);
		if ((res1 == 1) && (res2 == 1)) {
			if (x1 == x2) {
				++length;
			}
			else {
				printf("Files are not equal: %i\n", length);
				return length;
			}
		}
		else if ((res1 != 1) && (res2 != 1)) { //ends of both files
			printf("--- Coefs in files are equal ---\n");
			return 0;
		}
		else { //end of one file
			printf("Files are not equal (different length): %i\n", length);
			return length;
		}
	}
}

int compareCoefFiles_names(char *fileName1, char *fileName2) {
	FILE *fp1, *fp2;
	fp2 = fopen(fileName2, "r");
	fp1 = fopen(fileName1, "r");
	int res = compareCoefFiles(fp1, fp2);
	fclose(fp1);
	fclose(fp2);
	return res;
}

int compareFileContents(FileContent *fc1, FileContent *fc2) {
	if (fc1->total != fc2->total) { //checking total number of elements
		printf("FileContents are not equal: fc1->total (%i) != fc2->total (%i)\n", fc1->total, fc2->total);
		return -1;
	}
	if (fc1->nLines != fc2->nLines) { //checking number of lines
		printf("FileContents are not equal: fc1->nLines (%i) != fc2->nLines (%i)\n", fc1->nLines, fc2->nLines);
		return -1;
	}
	for (int line = 0; line < fc1->nLines; ++line) {
		if (fc1->lines[line].len != fc2->lines[line].len) { //checking lengthes of lines
			printf("FileContents are not equal: fc1->lines[%i].len(%i) != fc2->lines[%i].len(%i)\n", line, fc1->lines[line].len, line, fc2->lines[line].len);
			return -1;
		}
		for (int i = 0; i < fc1->lines[line].len; ++i) { //checking elements equality
			if (fc1->lines[line].vals[i] != fc2->lines[line].vals[i]) {
				printf("FileContents are not equal: line %i, num #%i\n", line, i);
				return -1;
			}
		}
	}
	printf("--- FileContents are equal ---\n");
	return 0;
}

void writeFileContent(FileContent *fc, FILE *fp) {
	for (int line = 0; line < fc->nLines; ++line) {
		for (int i = 0; i < fc->lines[line].len; ++i) {
			fprintf(fp, "%i ", fc->lines[line].vals[i]);
		}
		fprintf(fp, "\n");
	}
}

void writeFileContent_name(char *fileName, FileContent *fc) {
	FILE *fp;
	fp = fopen(fileName, "w");
	writeFileContent(fc, fp);
	fclose(fp);
}

void freeFileContent(FileContent *fc) {
	for (int line = 0; line < fc->nLines; ++line) {
		freeInts(&(fc->lines[line]));
	}
	free(fc->lines);
}





int bitlen(uint64_t x) {
	int len = 0;
	while (x > 0) {
		x >>= 1;
		++len;
	}
	return len;
}


Bits golomb(int x) {
	Bits res = emptyBits();
	int sign = (x <= 0) ? 1 : 0; // 0 -> '1'
	int abs = sign ? -x : x;
	int nBits = bitlen((uint64_t)abs);
	addNBitsToBits(&res, 0, nBits);
	addNBitsToBits(&res, abs, nBits); //this sequence of bits starts with '1'
	addBitToBits(&res, sign); // '1' if x = 0
	return res;
}
Bits golomb_short(int x, int np) {
	Bits res = emptyBits();
	int sign = (x < 0) ? 1 : 0;
	int abs = sign ? -x : x;
	abs -= np;
	if (abs == 0) {
		addBitToBits(&res, 1); //symbol of zero
	}
	else {
		int nBits = bitlen((uint64_t)abs);
		addNBitsToBits(&res, 0, nBits);
		addNBitsToBits(&res, abs, nBits); //this sequence of bits starts with '1'
	}
	addBitToBits(&res, sign);
	return res;
}


int readGolombFromFile(Bits *b, FILE *fp, int *res) {
	int nReadBytes = 0;
	Bits golombCode = emptyBits();
	int tmp;
	while (golombCode.data == 0) { //reading zeros and first '1'
		nReadBytes += readNBitsFromFileToInt(b, fp, 1, &tmp);
		addBitToBits(&golombCode, tmp);
	}
	if (golombCode.len == 1) { //golombCode begins with _1_
		*res = 0;
		return nReadBytes;
	}
	nReadBytes += readNBitsFromFileToInt(b, fp, golombCode.len - 2, &tmp);
	addNBitsToBits(&golombCode, tmp, golombCode.len - 2);
	nReadBytes += readNBitsFromFileToInt(b, fp, 1, &tmp); //reading sign
	*res = (int)golombCode.data;
	*res = tmp ? -*res : *res;
	return nReadBytes;
}


int readGolombFromFile_short(Bits *b, FILE *fp, int *res, int np) {
	int nReadBytes = 0;
	Bits golombCode = emptyBits();
	int tmp;
	while (golombCode.data == 0) { //reading zeros and first '1'
		nReadBytes += readNBitsFromFileToInt(b, fp, 1, &tmp);
		addBitToBits(&golombCode, tmp);
	}
	int abs;
	if (golombCode.len > 1) { //first '1' - most significant bit of abs
		nReadBytes += readNBitsFromFileToInt(b, fp, golombCode.len - 2, &tmp);
		addNBitsToBits(&golombCode, tmp, golombCode.len - 2);
		abs = (int)golombCode.data + np;
	}
	else { //first bit = '1' - symbol of zero
		abs = np;
	}
	nReadBytes += readNBitsFromFileToInt(b, fp, 1, &tmp); //reading sign
	*res = tmp ? -abs : abs;
	return nReadBytes;
}


int readGolombFromFile_short2(Bits *b, FILE *fp, int *res, int *base) {
	int nReadBytes = 0;
	Bits golombCode = emptyBits();
	int tmp;
	while (golombCode.data == 0) { //reading zeros and first '1'
		nReadBytes += readNBitsFromFileToInt(b, fp, 1, &tmp);
		addBitToBits(&golombCode, tmp);
	}
	int abs;
	if (golombCode.len > 1) { //first bit '1' - most significant bit of abs
		nReadBytes += readNBitsFromFileToInt(b, fp, golombCode.len - 2, &tmp);
		addNBitsToBits(&golombCode, tmp, golombCode.len - 2);
		abs = (int)golombCode.data + *base;
	}
	else { //first bit '1' - symbol of zero
		abs = *base; //= 0 + *base
	}
	nReadBytes += readNBitsFromFileToInt(b, fp, 1, &tmp); //reading sign
	*res = tmp ? -abs : abs; //applying sign
	*base = abs; //changing base
	return nReadBytes;
}


int readGolombFromFile_natNum(Bits *b, FILE *fp, int *res) {
	int nReadBytes = 0;
	Bits golombCode = emptyBits();
	int tmp;
	while (golombCode.data == 0) { //reading zeros and first '1'
		nReadBytes += readNBitsFromFileToInt(b, fp, 1, &tmp);
		addBitToBits(&golombCode, tmp);
	}
	//if (golombCode.len > 1) { //first bit '1' - most significant bit of res
	//	nReadBytes += readNBitsFromFileToInt(b, fp, golombCode.len - 1, &tmp);
	//	addNBitsToBits(&golombCode, tmp, golombCode.len - 1);
	//	*res = (int)golombCode.data;
	//}
	//else { //first bit '1' - symbol of one
	//	*res = 1;
	//}
	nReadBytes += readNBitsFromFileToInt(b, fp, golombCode.len - 1, &tmp);
	addNBitsToBits(&golombCode, tmp, golombCode.len - 1);
	*res = (int)golombCode.data;

	return nReadBytes;
}


void splitEsMant(int number, int *es, Bits *mant) {
	if (number >= -1 && number <= 1) {
		*es = number;
		*mant = emptyBits();
		return;
	}
	int sign = (number < 0); //1 - true, 0 - false
	int absNumber = sign ? -number : number; //absolute value
	int e = bitlen((uint64_t)absNumber); //number of bits in abs
	*es = sign ? -e : e; //writing new value to the matrix //signed number of bits in abs 
	mant->len = e - 1; //= bitlen(abs) - 1
	// mant->data = absNumber - (1 << mant->len); //bits of abs except high bit "1"
	mant->data = absNumber & (~(~0 << mant->len));
}


void splitEsMant2(int number, int *es2, Bits *mant2) {
	if (number >= -3 && number <= 3) {
		*es2 = number;
		*mant2 = emptyBits();
		return;
	}

	int sign = (number < 0); //1 - true, 0 - false
	int absNumber = sign ? -number : number; //absolute value
	int e = bitlen((uint64_t)absNumber); //number of bits in abs
	mant2->len = e - 2;
	mant2->data = absNumber & (~(~0 << mant2->len));

	int e2 = (e << 1) | ((absNumber >> (mant2->len)) & 0x1); //6, 7, 8, ...
	e2 -= 2; //4, 5, 6, ...
	*es2 = sign ? -e2 : e2;
}


void splitEsMant3(int number, int *es3, Bits *mant3) { ///////////////////////////
	if (number >= -7 && number <= 7) {
		*es3 = number;
		*mant3 = emptyBits();
		return;
	}

	int sign = (number < 0); //1 - true, 0 - false
	int absNumber = sign ? -number : number; //absolute value
	int e = bitlen((uint64_t)absNumber); //number of bits in abs
	mant3->len = e - 3;
	mant3->data = absNumber & (~(~0 << mant3->len));

	int e3 = (e << 2) | ((absNumber >> (mant3->len)) & 0x3); //16, 17, 18, ...
	e3 -= 8; //8, 9, 10, ...
	*es3 = sign ? -e3 : e3;
}


void sepEsMants(Group *gr, Subband *sbs, md_t mode, Bytes *mants) {
	if (mode != mode_exps && mode != mode_exps2 && mode != mode_exps3) {
		printf("mode exps mants incorrect!\n");
		exit(1);
	}
	*mants = allocBytesLen(BYTES_ALLOC_STEP);
	for (int sb_i = 0; sb_i < gr->sbNums.len; ++sb_i) {
		int sbNum = gr->sbNums.vals[sb_i];
		Subband *sb = &(sbs[sbNum]); //current subband

		for (int row = 0; row < sb->matr.nRows; ++row) {
			int *row_ptr = sb->matr.matrix[row];
			for (int col = 0; col < sb->matr.nColumns; ++col) {
				int *coef_p = row_ptr + col;

				Bits mant; //all bits of abs except most significant 1
				if (mode == mode_exps) {
					splitEsMant(*coef_p, coef_p, &mant); //coef -> es
				} else if (mode == mode_exps2) {
					splitEsMant2(*coef_p, coef_p, &mant); //coef -> es
				} else {
					splitEsMant3(*coef_p, coef_p, &mant); //coef -> es
				}
				addBitsToBytes(mants, mant); //writing mant	
			}
		}
	}
}


void sepEsMants_sb(Subband *sb, md_t mode, Bytes *mants) {
	if (mode != mode_exps && mode != mode_exps2 && mode != mode_exps3) {
		printf("mode exps mants incorrect!\n");
		exit(1);
	}
	*mants = allocBytesLen(BYTES_ALLOC_STEP);
	for (int row = 0; row < sb->matr.nRows; ++row) {
		int *row_ptr = sb->matr.matrix[row];
		for (int col = 0; col < sb->matr.nColumns; ++col) {
			int *coef_p = row_ptr + col;

			Bits mant; //all bits of abs except most significant 1
			if (mode == mode_exps) {
				splitEsMant(*coef_p, coef_p, &mant); //coef -> es
			} else if (mode == mode_exps2) {
				splitEsMant2(*coef_p, coef_p, &mant); //coef -> es
			} else { //if (mode == mode_exps3)
				splitEsMant3(*coef_p, coef_p, &mant); //coef -> es
			}
			addBitsToBytes(mants, mant); //writing mant	
		}
	}
}



void appointGroups(ExtMode *md, int _case) { 
	if (!md->useGroups) {
		return;
	}
	
	if (_case == 0) { //every sb in its own group
		md->nSbGroups = N_SUBBANDS_ALL;
		md->sbGroups = malloc(md->nSbGroups * sizeof(Ints));
		for (int i = 0; i < md->nSbGroups; ++i) {
			md->sbGroups[i] = allocIntsLen(1);
			md->sbGroups[i].vals[0] = i;
		}
	}

	else if (_case == 1) { //7 groups
		md->nSbGroups = 7;
		md->sbGroups = malloc(md->nSbGroups * sizeof(Ints));

		md->sbGroups[0] = allocIntsLen(1); //0
		md->sbGroups[0].vals[0] = 0;

		md->sbGroups[1] = allocIntsLen(2); //1,2
		md->sbGroups[1].vals[0] = 1;
		md->sbGroups[1].vals[1] = 2;

		md->sbGroups[2] = allocIntsLen(2); //3,6
		md->sbGroups[2].vals[0] = 3;
		md->sbGroups[2].vals[1] = 6;

		md->sbGroups[3] = allocIntsLen(4); //4,5,7,8
		md->sbGroups[3].vals[0] = 4;
		md->sbGroups[3].vals[1] = 5;
		md->sbGroups[3].vals[2] = 7;
		md->sbGroups[3].vals[3] = 8;

		md->sbGroups[4] = allocIntsLen(2); //9,10
		md->sbGroups[4].vals[0] = 9;
		md->sbGroups[4].vals[1] = 10;

		md->sbGroups[5] = allocIntsLen(2); //11,14
		md->sbGroups[5].vals[0] = 11;
		md->sbGroups[5].vals[1] = 14;

		md->sbGroups[6] = allocIntsLen(4); //12,13,15,16
		md->sbGroups[6].vals[0] = 12;
		md->sbGroups[6].vals[1] = 13;
		md->sbGroups[6].vals[2] = 15;
		md->sbGroups[6].vals[3] = 16;
	}

	else if (_case == 2) { //9 groups
		md->nSbGroups = 9;
		md->sbGroups = malloc(md->nSbGroups * sizeof(Ints));

		md->sbGroups[0] = allocIntsLen(1); //0
		md->sbGroups[0].vals[0] = 0;

		md->sbGroups[1] = allocIntsLen(2); //1,2
		md->sbGroups[1].vals[0] = 1;
		md->sbGroups[1].vals[1] = 2;

		md->sbGroups[2] = allocIntsLen(2); //3,6
		md->sbGroups[2].vals[0] = 3;
		md->sbGroups[2].vals[1] = 6;

		md->sbGroups[3] = allocIntsLen(1); //4
		md->sbGroups[3].vals[0] = 4;

		md->sbGroups[4] = allocIntsLen(3); //5,7,8
		md->sbGroups[4].vals[0] = 5;
		md->sbGroups[4].vals[1] = 7;
		md->sbGroups[4].vals[2] = 8;

		md->sbGroups[5] = allocIntsLen(2); //9,10
		md->sbGroups[5].vals[0] = 9;
		md->sbGroups[5].vals[1] = 10;

		md->sbGroups[6] = allocIntsLen(2); //11,14
		md->sbGroups[6].vals[0] = 11;
		md->sbGroups[6].vals[1] = 14;

		md->sbGroups[7] = allocIntsLen(1); //12
		md->sbGroups[7].vals[0] = 12;

		md->sbGroups[8] = allocIntsLen(3); //13,15,16
		md->sbGroups[8].vals[0] = 13;
		md->sbGroups[8].vals[1] = 15;
		md->sbGroups[8].vals[2] = 16;
	}

	else if (_case == 3) { //9 groups //родитель-наследник
		md->nSbGroups = 9;
		md->sbGroups = malloc(md->nSbGroups * sizeof(Ints));

		md->sbGroups[0] = allocIntsLen(1); //0
		md->sbGroups[0].vals[0] = 0;

		md->sbGroups[1] = allocIntsLen(2); //1,9
		md->sbGroups[1].vals[0] = 1;
		md->sbGroups[1].vals[1] = 9;

		md->sbGroups[2] = allocIntsLen(2); //2,10
		md->sbGroups[2].vals[0] = 2;
		md->sbGroups[2].vals[1] = 10;

		md->sbGroups[3] = allocIntsLen(2); //3,11
		md->sbGroups[3].vals[0] = 3;
		md->sbGroups[3].vals[1] = 11;

		md->sbGroups[4] = allocIntsLen(2); //6,14
		md->sbGroups[4].vals[0] = 6;
		md->sbGroups[4].vals[1] = 14;

		md->sbGroups[5] = allocIntsLen(2); //4,12
		md->sbGroups[5].vals[0] = 4;
		md->sbGroups[5].vals[1] = 12;

		md->sbGroups[6] = allocIntsLen(2); //5,13
		md->sbGroups[6].vals[0] = 5;
		md->sbGroups[6].vals[1] = 13;

		md->sbGroups[7] = allocIntsLen(2); //7,15
		md->sbGroups[7].vals[0] = 7;
		md->sbGroups[7].vals[1] = 15;

		md->sbGroups[8] = allocIntsLen(2); //8,16
		md->sbGroups[8].vals[0] = 8;
		md->sbGroups[8].vals[1] = 16;
		
	}
}


void freeMdGroups(ExtMode *md) {
	if (!md->useGroups) {
		return;
	}
	for (int i = 0; i < md->nSbGroups; ++i) {
		freeInts(&(md->sbGroups[i]));
	}
	free(md->sbGroups);
}



int getNumOfContTypes(contType_t contType) { //number of contexts for 1 symbol (coefficient)
	switch (contType) {
	case z_nz:			return 2;
	case n_z_p:			return 3;
	case n_n1_z_p1_p:	return 5;
	case z_a1_a:		return 3;
	case ez_nz:			return 2;
	case ez2_nz:		return 2;
	case n_n12_z_p12_p:	return 5;
	case z_a12_a:		return 3;
	case n_ez_p:		return 3;
	case n_ez2_p:		return 3;
	default:			return -1;
	}
}

int numOfBaseSymbols(contBase_t contBase) { //number of base symbols
	switch (contBase) {
	case base_noCont:		return 0;
	case base_L:			return 1;
	case base_D:			return 1;
	case base_T:			return 1;
	case base_R:			return 1;
	case base_LD:			return 2;
	case base_DT:			return 2;
	case base_LT:			return 2;
	case base_LR:			return 2;
	case base_DR:			return 2;
	case base_LDT:			return 3;
	case base_LTR:			return 3;
	case base_LDTR:			return 4;
	case base_sumLT:		return 1;
	case base_sumLDTR:		return 1;
	case base_sumLL2:		return 1;
	case base_sumTT2:		return 1;
	case base_L2T2:			return 2;
	case base_sumLL2sumTT2: return 2;
	case base_LL2TT2:		return 4;
	case base_TsumLL2:		return 2;
	case base_TLL2:			return 3;
	case base_LsumTT2:		return 2;
	case base_LTT2:			return 3;
	case base_LsumDT:		return 2;
	case base_LsumDTR:		return 2;
	case base_TsumLD:		return 2;
	default:				return -1;
	}
}

//createContMap_equal -- присвоение всем суббэндам одинаковой контекстной модели
void createContMap_equal(ExtMode *md, contBase_t base, contType_t type, int isParentUsage, contType_t pType) {
	md->contModelsAreDifferent = 0;
	md->contMap = NULL;
	
	md->uniformContModel.base = base;
	
	md->uniformContModel.typesAreDifferent = 0;
	md->uniformContModel.types = NULL;
	md->uniformContModel.uniformType = type;

	md->uniformContModel.isParentUsage = isParentUsage;
	md->uniformContModel.pType = pType;
}

void createContMap_individual(ExtMode *md, contBase_t base, contType_t *types /*this is the difference*/, int isParentUsage, contType_t pType) {
	int nBaseSyms = numOfBaseSymbols(base);
	int contMapLen = (md->useGroups) ? md->nSbGroups : N_SUBBANDS_ALL;
	md->contMap = malloc(contMapLen * sizeof(ContModel));
	for (int i = 0; i < contMapLen; ++i) { //filling cont map
		md->contMap[i].base = base;
		md->contMap[i].typesAreDifferent = 1;
		md->contMap[i].types = malloc(nBaseSyms);
		for (int j = 0; j < nBaseSyms; ++j) {
			md->contMap[i].types[j] = types[j];
		}
		md->contMap[i].isParentUsage = isParentUsage;
		md->contMap[i].pType = pType;
	}
}

void freeContMap(ExtMode *md) {
	if (md->contModelsAreDifferent) {
		int contMapLen = (md->useGroups) ? md->nSbGroups : N_SUBBANDS_ALL;
		for (int i = 0; i < contMapLen; ++i) { //filling cont map
			if (md->contMap[i].typesAreDifferent) {
				free(md->contMap[i].types);
			}
		}
		free(md->contMap);
	}
}

int getNumOfConts(ContModel cm) {
	int nBaseSyms = numOfBaseSymbols(cm.base);
	int nConts = 1;
	if (cm.typesAreDifferent) {
		for (int i = 0; i < nBaseSyms; ++i) {
			nConts *= getNumOfContTypes(cm.types[i]);
		}
	}
	else {
		int numOfContTypes = getNumOfContTypes(cm.uniformType);
		for (int i = 0; i < nBaseSyms; ++i) {
			nConts *= numOfContTypes;
		}
	}
	return nConts;
}

int getSymbolCont(int sym, contType_t contType) {
	switch (contType) {
	case z_nz:			return (sym == 0) ? 0 : 1;
	case n_z_p:			return (sym < 0) ? 1 : (sym == 0) ? 0 : 2;
	case n_n1_z_p1_p:	return (sym < -1) ? 0 : (sym == -1) ? 1 : (sym == 0) ? 2 : (sym == 1) ? 3 : 4;
	case z_a1_a:		return (sym == 0) ? 0 : ((sym == -1) || (sym == 1)) ? 1 : 2;
	case ez_nz:			return ((sym >= -1) && (sym <= 1)) ? 0 : 1;
	case ez2_nz:		return ((sym >= -2) && (sym <= 2)) ? 0 : 1;
	case n_n12_z_p12_p:	return (sym < -2) ? 3 : (sym >= -2) && (sym <= -1) ? 1 : (sym == 0) ? 0 : (sym >= 1) && (sym <= 2) ? 2 : 4;
	case z_a12_a:		return (sym == 0) ? 0 : ((sym >= -2) && (sym <= 2)) ? 1 : 2;
	case n_ez_p:		return (sym < -1) ? 1 : ((sym >= -1) && (sym <= 1)) ? 0 : 2;
	case n_ez2_p:		return (sym < -2) ? 1 : ((sym >= -2) && (sym <= 2)) ? 0 : 2;
	default:			return -1;
	}
}


Matrix emptyMatrix() {
	Matrix m;
	m.nColumns = 0;
	m.nRows = 0;
	m.matrix = NULL;
	return m;
}

Matrix createMatrixWH(int w, int h) {
	Matrix m;
	m.nColumns = w;
	m.nRows = h;
	m.matrix = malloc(m.nRows * sizeof(int*));
	for (int row = 0; row < m.nRows; ++row) {
		m.matrix[row] = malloc(m.nColumns * sizeof(int));
	}
	return m;
}


Matrix createMatrixFromInts(const Ints* syms, int w, int h) {
	if (syms->len != w * h) {
		printf("syms.len(%i) != w(%i) * h(%i)", syms->len, w, h);
		exit(1);
	}
	Matrix m = createMatrixWH(w, h);
	int cnt = 0;
	for (int row = 0; row < m.nRows; ++row) {
		for (int col = 0; col < m.nColumns; ++col) {
			m.matrix[row][col] = syms->vals[cnt++];
		}
	}
	return m;
}

void freeMartix(Matrix *m) {
	for (int row = 0; row < m->nRows; ++row) {
		free(m->matrix[row]);
	}
	free(m->matrix);
	m->matrix = NULL;
	m->nColumns = 0;
	m->nRows = 0;
}


int getContext(Matrix m, int row, int col, ContModel cm) {
	int L = 0, T = 0, D = 0, R = 0, L2 = 0, T2 = 0;
	
	L = (col == 0) ? 0 : m.matrix[row][col - 1];	//left           
	T = (row == 0) ? 0 : m.matrix[row - 1][col];	//top
	
	if (!cm.typesAreDifferent && cm.base == base_LT && cm.uniformType == n_z_p) {
		int index_L = (L == 0) ? 0 : (L < 0) ? 1 : 2;
		int index_T = (T == 0) ? 0 : (T < 0) ? 1 : 2;
		static const int map[3][3] = {{0,1,2}, {3,5,6}, {4,7,8}};
		return map[index_L][index_T];
	}

	if (cm.base == base_D
	|| cm.base == base_LD
	|| cm.base == base_DT
	|| cm.base == base_DR
	|| cm.base == base_LDT
	|| cm.base == base_LDTR
	|| cm.base == base_sumLDTR
	|| cm.base == base_LsumDT
	|| cm.base == base_LsumDTR
	|| cm.base ==base_TsumLD
	) {
		D = ((col == 0) || (row == 0)) ? 0 : m.matrix[row - 1][col - 1];	//diag top left
	}

	if (cm.base == base_R 
	|| cm.base == base_LR 
	|| cm.base == base_DR 
	|| cm.base == base_LTR 
	|| cm.base == base_LDTR
	|| cm.base == base_sumLDTR 
	|| cm.base == base_LsumDTR
	) {
		R = ((col == (m.nColumns - 1)) || (row == 0)) ? 0 : m.matrix[row - 1][col + 1];	//diag top right
	}

	if (cm.base == base_sumLL2 
	|| cm.base == base_L2T2 
	|| cm.base == base_sumLL2sumTT2 
	|| cm.base == base_LL2TT2
	|| cm.base == base_TsumLL2 
	|| cm.base == base_TLL2
	) {
		L2 = ((col == 0) || (col == 1)) ? 0 : m.matrix[row][col - 2];	//left left
	}

	if (cm.base == base_sumTT2 
	|| cm.base == base_L2T2 
	|| cm.base == base_sumLL2sumTT2 
	|| cm.base == base_LL2TT2
	|| cm.base == base_LsumTT2 
	|| cm.base == base_LTT2
	) {
		T2 = ((row == 0) || (row == 1)) ? 0 : m.matrix[row - 2][col];	//top top
	}


	int nBaseVals = numOfBaseSymbols(cm.base);
	int bv[nBaseVals]; //base vals array

	switch (cm.base) {
	case base_noCont:		break;
	case base_L:			bv[0] = L;												break;
	case base_D:			bv[0] = D;												break;
	case base_T:			bv[0] = T;												break;
	case base_R:			bv[0] = R;												break;
	case base_LD:			bv[0] = L;		bv[1] = D;								break;
	case base_DT:			bv[0] = D;		bv[1] = T;								break;
	case base_LT:			bv[0] = L;		bv[1] = T;								break;
	case base_LR:			bv[0] = L;		bv[1] = R;								break;
	case base_DR:			bv[0] = D;		bv[1] = R;								break;
	case base_LDT:			bv[0] = L;		bv[1] = D;		bv[2] = T;				break;
	case base_LTR:			bv[0] = L;		bv[1] = T;		bv[2] = R;				break;
	case base_LDTR:			bv[0] = L;		bv[1] = D;		bv[2] = T;	bv[3] = R;	break;
	case base_sumLT:		bv[0] = L + T;											break;
	case base_sumLDTR:		bv[0] = L + D + T + R;									break;
	case base_sumLL2:		bv[0] = L + L2;											break;
	case base_sumTT2:		bv[0] = T + T2;											break;
	case base_L2T2:			bv[0] = L2;		bv[1] = T2;								break;
	case base_sumLL2sumTT2: bv[0] = L + L2;	bv[1] = T + T2;							break;
	case base_LL2TT2:		bv[0] = L;		bv[1] = L2;		bv[2] = T;	bv[3] = T2;	break;
	case base_TsumLL2:		bv[0] = T;		bv[1] = L + L2;							break;
	case base_TLL2:			bv[0] = T;		bv[1] = L;		bv[2] = L2;				break;
	case base_LsumTT2:		bv[0] = L;		bv[1] = T + T2;							break;
	case base_LTT2:			bv[0] = L;		bv[1] = T;		bv[2] = T2;				break;
	case base_LsumDT:		bv[0] = L;		bv[1] = D + T;							break;
	case base_LsumDTR:		bv[0] = L;		bv[1] = D + T + R;						break;
	case base_TsumLD:		bv[0] = T;		bv[1] = L + D;							break;
	default:				return -1;
	}

	int cont = 0;
	if (cm.typesAreDifferent) {
		for (int i = 0; i < nBaseVals; ++i) {
			cont *= getNumOfContTypes(cm.types[i]);
			cont += getSymbolCont(bv[i], cm.types[i]);
		}
	} 
	else {
		int numOfContTypes = getNumOfContTypes(cm.uniformType);
		for (int i = 0; i < nBaseVals; ++i) {
			cont *= numOfContTypes;
			cont += getSymbolCont(bv[i], cm.uniformType);
		}
	}
	return cont;
}

void considerParent(int *cont, Subband *sb, int row, int col, contType_t pType) {
	if (sb->parentPointer == NULL) {
		printf("sb.parentPointer == NULL\n");
		exit(1);
	}
	Subband *parent = sb->parentPointer;
	Matrix m = sb->matr;
	int parentRow = row / 3;
	int parentCol = col / 3;
	int P = parent->matr.matrix[parentRow][parentCol];
	
	*cont *= getNumOfContTypes(pType);
	*cont += getSymbolCont(P, pType);
}
void considerParent_dec(int *cont, Subband *sb, int row, int col, contType_t pType, md_t mode) {
	if (sb->parentPointer == NULL) {
		printf("sb.parentPointer == NULL\n");
		exit(1);
	}
	Subband *parent = sb->parentPointer;
	Matrix m = sb->matr;
	int parentRow = row / 3;
	int parentCol = col / 3;
	int P = parent->matr.matrix[parentRow][parentCol];

	Bits mant; //all bits of abs except most significant 1
	if (mode == mode_exps) {
		splitEsMant(P, &P, &mant); //coef -> es
	} else if (mode == mode_exps2) {
		splitEsMant2(P, &P, &mant); //coef -> es
	} else {
		splitEsMant3(P, &P, &mant); //coef -> es
	}
	*cont *= getNumOfContTypes(pType);
	*cont += getSymbolCont(P, pType);
}



void makeNums_gr(Group gr, Subband *sbs, Ints *nums, Ints *contFlags, Dict *dicts) {
	int nSymsGr = 0; //total number of symbols in all subbands of group
	for (int i = 0; i < gr.sbNums.len; ++i) {
		int sbNum = gr.sbNums.vals[i];
		nSymsGr += sbs[sbNum].size;
	}
	*nums = allocIntsLen(nSymsGr); //symbols from sym[], converted to nums
	*contFlags = allocIntsLen(nSymsGr);
	const int SYMS_ALLOC_STEP = 50;

	int nConts = gr.nConts;
	int allocatedSizes[nConts]; //allocated sizes of dicts
	for (int cont = 0; cont < nConts; ++cont) {
		allocatedSizes[cont] = SYMS_ALLOC_STEP;
		dicts[cont].syms = malloc(allocatedSizes[cont] * sizeof(Sym));
		dicts[cont].len = 0;
		dicts[cont].sumOfFreqs = 0;
	}

	int n = 0; //number of processed symbols
	for (int sb_i = 0; sb_i < gr.sbNums.len; ++sb_i) {
		int sbNum = gr.sbNums.vals[sb_i];
		Subband *sb = &(sbs[sbNum]);

		for (int row = 0; row < sb->matr.nRows; ++row) {
			for (int col = 0; col < sb->matr.nColumns; ++col) {
				int cont = getContext(sb->matr, row, col, gr.cm);
				if (gr.cm.isParentUsage && gr.sbsType == sb_1) {
					considerParent(&cont, sb, row, col, gr.cm.pType);
				}

				contFlags->vals[n] = cont; //writing context
				Dict *dict = &(dicts[cont]); //choosing dict
				++(dict->sumOfFreqs);

				int currentSymbol = sb->matr.matrix[row][col];
				int found = 0; //is symbol found in dict
				for (int j = 0; j < dict->len; ++j) { //passing through dict and searching current symbol
					if (dict->syms[j].sym == currentSymbol) {
						found = 1;
						++(dict->syms[j].freq);
						nums->vals[n++] = j;
						break;
					}
				}
				if (!found) { //if new symbol
					if ((dict->len + 1) > allocatedSizes[cont]) { //if need to alloc more memory
						allocatedSizes[cont] += SYMS_ALLOC_STEP;
						dict->syms = realloc(dict->syms, allocatedSizes[cont] * sizeof(Sym));
					}
					dict->syms[dict->len].num = dict->len;
					dict->syms[dict->len].sym = currentSymbol;
					dict->syms[dict->len].freq = 1;
					nums->vals[n++] = dict->len++; //writing to nums and updating dictLen
				}
			}
		}
	}
}


void makeNums_sb(Subband *sb, Ints *nums, Ints *contFlags, Dict *dicts) {
	*nums = allocIntsLen(sb->size); //symbols from sym[], converted to nums
	*contFlags = allocIntsLen(sb->size);
	const int SYMS_ALLOC_STEP = 50;

	int nConts = sb->nConts;
	int allocatedSizes[nConts]; //allocated sizes of dicts
	for (int cont = 0; cont < nConts; ++cont) {
		allocatedSizes[cont] = SYMS_ALLOC_STEP;
		dicts[cont].syms = malloc(allocatedSizes[cont] * sizeof(Sym));
		// memset(dicts[cont].syms, 0, allocatedSizes[cont] * sizeof(Sym)); //////////
		dicts[cont].len = 0;
		dicts[cont].sumOfFreqs = 0;
	}

	int n = 0; //number of processed symbols
	for (int row = 0; row < sb->matr.nRows; ++row) {
		for (int col = 0; col < sb->matr.nColumns; ++col) {
			int cont = getContext(sb->matr, row, col, sb->cm);
			if (sb->cm.isParentUsage && sb->sbType == sb_1) {
				considerParent(&cont, sb, row, col, sb->cm.pType);
			}

			contFlags->vals[n] = cont; //writing context
			Dict *dict = &(dicts[cont]); //choosing dict
			++(dict->sumOfFreqs);
			
			int currentSymbol = sb->matr.matrix[row][col];
			int found = 0; //is symbol found in dict
			for (int j = 0; j < dict->len; ++j) { //passing through dict and searching current symbol
				if (dict->syms[j].sym == currentSymbol) {
					found = 1;
					++(dict->syms[j].freq);
					nums->vals[n++] = j;
					break;
				}
			}
			if (!found) { //if new symbol
				if ((dict->len + 1) > allocatedSizes[cont]) { //if need to alloc more memory
					allocatedSizes[cont] += SYMS_ALLOC_STEP;
					dict->syms = realloc(dict->syms, allocatedSizes[cont] * sizeof(Sym));
				}
				dict->syms[dict->len].num = dict->len;
				dict->syms[dict->len].sym = currentSymbol;
				dict->syms[dict->len].freq = 1;
				nums->vals[n++] = dict->len++; //writing to nums and updating dictLen
			}
		}
	}
}


void changeNums(Ints *nums, const Ints* contFlags, int **mappingOldToNew) {
	for (int i = 0; i < nums->len; ++i) {
		int *transMap = mappingOldToNew[contFlags->vals[i]]; //choosing map according to context (dict)
		nums->vals[i] = transMap[nums->vals[i]]; //transform
	}
}




double entropy_dict(Dict *dict) {
	int len = 0;
	for (int i = 0; i < dict->len; ++i) {
		len += dict->syms[i].freq;
	}

	//entropy calculation
	double H = 0;
	for (int i = 0; i < dict->len; ++i) {
		if (dict->syms[i].freq != 0) {
			double p = (double)dict->syms[i].freq / len;
			H -= p * log(p) / log(256);
		}
	}

	return H;
}


Dict allocDictLen(int len) {
	Dict d;
	d.sumOfFreqs = 0;
	d.len = len;
	d.syms = malloc(d.len * sizeof(Sym));
	return d;
}


Dict* allocEmptyDicts(int nDicts) {
	Dict *dicts = malloc(nDicts * sizeof(Dict));
	memset(dicts, 0, nDicts * sizeof(Dict));
	return dicts;
}

// void createDict(const Ints* syms, Dict *dictP) {
// 	Sym *dict = malloc(syms->len * sizeof(Sym));
// 	int dictLen = 0; //number of different symbols

// 	for (int i = 0; i < syms->len; ++i) { //passing through array sym[]
// 		int currentSymbol = syms->vals[i];
// 		int found = 0; //is symbol found in dict
// 		for (int j = 0; j < dictLen; ++j) { //passing through dict[]
// 			if (dict[j].sym == currentSymbol) {
// 				found = 1;
// 				++dict[j].freq;
// 				break;
// 			}
// 		}
// 		if (!found) {
// 			dict[dictLen].num = dictLen;
// 			dict[dictLen].sym = currentSymbol;
// 			dict[dictLen].freq = 1;
// 			++dictLen;
// 		}
// 	}

// 	dictP->syms = realloc(dict, dictLen * sizeof(Sym)); //память dict теперь принадлежит dictP->syms
// 	dictP->len = dictLen;
// }

Dict makeNormDict(const Dict* dict, const int PROBABILITY_PRECISION) { //adapting ezcs_CollectStatistics_2 for dict
	Dict normDict = allocDictLen(dict->len);
	
	if (dict->len == 0) {
		return normDict;
	}
	int freqs[dict->len];
	ezcs_CollectStatistics_2(freqs, dict, PROBABILITY_PRECISION); //normalization
	
	for (int i = 0; i < dict->len; ++i) {
		normDict.syms[i] = dict->syms[i]; //copy from dict all fields of sym
		normDict.syms[i].freq = freqs[i]; //change field freq
		normDict.sumOfFreqs += normDict.syms[i].freq;
	}
	return normDict;
}

// Dict makeNormDict_FSEnormalization(const Dict* dict, const int PROBABILITY_PRECISION) { //adapting ezcs_CollectStatistics_2 for dict
// 	Dict normDict = allocDictLen(dict->len);
	
// 	if (dict->len == 0) {
// 		return normDict;
// 	}
// 	// int freqs[dict->len];
// 	short freqs[dict->len];
// 	unsigned count[dict->len];
// 	for (int i = 0; i < dict->len; ++i) {
// 		count[i] = dict->syms[i].freq;
// 	}
// 	FSE_normalizeCount(freqs, PROBABILITY_PRECISION, count, dict->sumOfFreqs, dict->len - 1);

// 	// int flag = 0;
// 	for (int i = 0; i < dict->len; ++i) {
// 		assert(freqs[i] != 0); ///////////////////////
// 		// if (freqs[i] == 0) { ///////////////////////
// 		// 	flag = 1;
// 		// }
// 		normDict.syms[i] = dict->syms[i]; //copy from dict all fields of sym
// 		normDict.syms[i].freq = freqs[i]; //change field freq
// 		normDict.sumOfFreqs += normDict.syms[i].freq;
// 	}
// 	// if (flag) { ////////////////////
// 	// 	printDict(normDict);
// 	// 	assert(0);
// 	// }
// 	return normDict;
// }

// void normalizeDict(Dict *dict, const int PROBABILITY_PRECISION) {
// 	if (dict->len == 0) return;
// 	int freqs[dict->len];
// 	ezcs_CollectStatistics_2(freqs, dict, PROBABILITY_PRECISION); //normalization
// 	int newSumOfFreqs = 0;
// 	for (int i = 0; i < dict->len; ++i) {
// 		dict->syms[i].freq = freqs[i]; //change field freq
// 		newSumOfFreqs += dict->syms[i].freq;
// 	}
// 	dict->sumOfFreqs = newSumOfFreqs;
// }

void copyDict(const Dict* src, Dict *dst) {
	dst->sumOfFreqs = src->sumOfFreqs;
	dst->len = src->len;
	dst->syms = malloc(dst->len * sizeof(Sym));
	// for (int i = 0; i < src->len; ++i) {
	// 	dst->syms[i] = src->syms[i];
	// }
	memcpy(dst->syms, src->syms, src->len * sizeof(Sym));
}

int compSyms_desc(const void* a, const void* b) { //descending (from max freq to min)
	return ((Sym*)b)->freq - ((Sym*)a)->freq;
}

int compSyms_asc(const void* a, const void* b) { //ascending (from min freq to max)
	return ((Sym*)a)->freq - ((Sym*)b)->freq;
}


void sortDict_getOrder(Dict *dict) {
	int nSymbols = dict->len;
	int freqs[nSymbols];
	for (int i = 0; i < nSymbols; ++i) {
		freqs[i] = dict->syms[i].freq;
	}

	int order[nSymbols];
	GetOrder(freqs, order, nSymbols); //порядок понадобится в makeChart (изначально GetOrder() вызывался в makeChart)

	Sym *sorted = malloc(nSymbols * sizeof(Sym));
	for (int i = 0; i < nSymbols; ++i) {
		sorted[i] = dict->syms[order[i]];
	}
	free(dict->syms);
	dict->syms = sorted;
}

Dict* makeNormalizedDicts(Dict *dicts, int nDicts) { 
	Dict *normDicts = malloc(nDicts * sizeof(Dict));
	int PROBABILITY_PRECISION = probPrec();
	for (int i = 0; i < nDicts; ++i) {
		normDicts[i] = makeNormDict(&(dicts[i]), PROBABILITY_PRECISION);
		// normDicts[i] = makeNormDict_FSEnormalization(dicts[i], PROBABILITY_PRECISION);
	}
	return normDicts;
}

Dict* makeNormalizedDicts_decEnv(Dict *dicts, int nDicts, const ExtMode* md) {
	if (md->headerNorm == hf_plain) {
		return makeNormalizedDicts(dicts, nDicts);
	}
	else { //without calling makeNormDict()
		Dict *normDicts = malloc(nDicts * sizeof(Dict));
		for (int i = 0; i < nDicts; ++i) {
			copyDict(&(dicts[i]), &(normDicts[i]));
		}
		return normDicts;
	}
}



int compSyms_abs(const void* a, const void* b) { //0, -1, 1, -2, 2, ...
	Sym *A = (Sym*)a;
	Sym *B = (Sym*)b;
	if (abs(A->sym) == abs(B->sym)) {
		return A->sym - B->sym;
	}
	return abs(A->sym) - abs(B->sym);
}



void renumberSyms(Dict *dicts, int nDicts, Ints *nums, const Ints* contFlags, const ExtMode *md, int nConts) {
	if (md->headerSort == hs_noSort) {
		return;
	}

	for (int i = 0; i < nDicts; ++i) {
		if (md->headerSort == hs_sortFreq) { //from max freq to min
			qsort(dicts[i].syms, dicts[i].len, sizeof(Sym), compSyms_desc);
		}
		else if (md->headerSort == hs_sortAbs) { //0,-1,1,-2,2,...
			qsort(dicts[i].syms, dicts[i].len, sizeof(Sym), compSyms_abs);
		}
	}

	//building mapping from old nums to new nums and applying it
	int *mapOldToNew[nDicts];
	for (int i = 0; i < nDicts; ++i) {
		mapOldToNew[i] = malloc(dicts[i].len * sizeof(int));
		for (int newNum = 0; newNum < dicts[i].len; ++newNum) {
			mapOldToNew[i][dicts[i].syms[newNum].num] = newNum;
			dicts[i].syms[newNum].num = newNum;
		}
	}
	changeNums(nums, contFlags, mapOldToNew);

	//free mapOldToNew
	for (int i = 0; i < nConts; ++i) {
		free(mapOldToNew[i]);
	}
}



void freeDictsAr(Dict **dicts, int nDicts) {
	if (*dicts == NULL) {
		return;
	}
	for (int i = 0; i < nDicts; ++i) {
		if ((*dicts)[i].syms) {
			free((*dicts)[i].syms);
			(*dicts)[i].syms = NULL;
		}
	}
	free(*dicts);
	*dicts = NULL;
}



int nextSymVal(int sym) {
	if (sym == 0) {
		return -1;
	}
	else if (sym < 0) {
		return -sym;
	}
	else /*if (sym > 0)*/ {
		return -(sym + 1);
	}
}



void writeDict_freqs_w0(const Dict* dict, Bytes *out, int *remainingSize) {
	addNBitsToBytes(out, dict->sumOfFreqs, bitlen(*remainingSize));
	*remainingSize -= dict->sumOfFreqs;
	int freqsLeft = dict->sumOfFreqs;
	int currentSym = 0;
	for (int i = 0; i < dict->len; ++i) {
		int bitsPerFreq = bitlen(freqsLeft);
		while (dict->syms[i].sym != currentSym) { //пропуск отсутствующих символов
			addNBitsToBytes(out, 0, bitsPerFreq);
			currentSym = nextSymVal(currentSym);
		}
		int freq = dict->syms[i].freq;
		addNBitsToBytes(out, freq, bitsPerFreq);
		freqsLeft -= freq;
		currentSym = nextSymVal(currentSym);
	}
}



void writeDict_freqsWithZeros(const Dict* dict, Bytes *out, int *remainingSize, int *lens) {
	Bytes prev = *out;//
	writeDict_freqs_w0(dict, out, remainingSize); //writing freqs
	lens[2] += deltaBits(*out, prev);//
}



int getGrSize(Group *gr, Subband *sbs) {
	int grSize = 0;
	for (int sb_i = 0; sb_i < gr->sbNums.len; ++sb_i) {
		int sbNum = gr->sbNums.vals[sb_i];
		grSize += sbs[sbNum].size;
	}
	return grSize;
}



void writeHeader(Bytes *out, const ExtMode *md, int totalSize, const Dict *dicts, const Dict *normDicts, int nDicts, int *hBits) {
	if (out->posBit != BYTE_LEN - 1) {
		printf("Bytes aren't aligned (out->posBit != 0)\n");
		exit(1);
	}

	const Dict *ds = (md->headerNorm == hf_plain) ? dicts : normDicts; //choosing type of freqs to write
	int remainingSize = totalSize;
	for (int i = 0; i < nDicts; ++i) {
		writeDict_freqsWithZeros(&(ds[i]), out, &remainingSize, hBits);
	}

	assert(remainingSize == 0);
}


void readDict_freqs_w0(Bytes *bt, int *remainingSize, Dict *dict) {
	const int SYMS_ALLOC_STEP = 20; //step of incrementing dictLen

	dict->sumOfFreqs = readNBitsFromBytes(bt, bitlen(*remainingSize));
	*remainingSize -= dict->sumOfFreqs; 
	int freqsLeft = dict->sumOfFreqs;
	int currentIndex = 0;
	int currentSym = 0; //0,-1,1,-2,2,...
	int dictCapacity = 0;
	while (freqsLeft > 0) {
		int bitsPerFreq = bitlen((uint64_t)freqsLeft);
		int freq = readNBitsFromBytes(bt, bitsPerFreq);
		if (freq != 0) {
			if (currentIndex == dictCapacity) {
				dictCapacity += SYMS_ALLOC_STEP;
				dict->syms = realloc(dict->syms, dictCapacity * sizeof(Sym));
			}
			dict->syms[currentIndex].num = currentIndex;
			dict->syms[currentIndex].sym = currentSym;
			dict->syms[currentIndex].freq = freq;
			++currentIndex;
			freqsLeft -= freq;
		}
		currentSym = nextSymVal(currentSym);
	}
	dict->len = currentIndex;
}



void readHeader(Bytes *bts, int remainingSize, Dict *dicts, int nDicts) { //reading dicts
	for (int i = 0; i < nDicts; ++i) {
		readDict_freqs_w0(bts, &remainingSize, &(dicts[i])); //reading freqs
	}
	assert(remainingSize == 0);
}




Subband emptySubband() {
	Subband sb;
	sb.matr = emptyMatrix();
	sb.size = 0;
	sb.w    = 0;
	sb.h    = 0;
	sb.sbNum = -1;
	sb.parentPointer = NULL;
	return sb;
}

void freeSb(Subband *sb) {
	freeMartix(&(sb->matr));
	*sb = emptySubband();
}

int getSubbandsSizes(uint64_t size, int *subband2_size, int *subband1_size) {
	switch (size) {
		case 409536: //704x576
			*subband2_size = 5056;
			*subband1_size = 9 * (*subband2_size);
			break;
		case 926640: //1280x720
			*subband2_size = 11440;
			*subband1_size = 9 * (*subband2_size);
			break;
		case 2080080: //1920x1080
			*subband2_size = 25680;
			*subband1_size = 9 * (*subband2_size);
			break;

		case 406464: //704x576 new
			*subband2_size = 5056;
			*subband1_size = 45120;
			break;
		case 922800: //1280x720 new
			*subband2_size = 11440;
			*subband1_size = 102480;
			break;
		case 2074320: //1920x1080 new
			*subband2_size = 25680;
			*subband1_size = 230400;
			break;
		default: //if incorrect size
			return -1;
	}
	return 0;
}

int getSubbandsWH(uint64_t size, int *sb2_w, int *sb2_h, int *sb1_w, int *sb1_h) {
	////const int subband2_size = ifc->lineSizes[0]; //first sb
	////const int subband1_size = ifc->lineSizes[ifc->nLines - 1]; //last sb
	//int subband2_size, subband1_size;
	//getSubbandsSizes(ifc->total, &subband2_size, &subband1_size);

	int frameW;
	int frameH;
	switch (size) {
	case 406464: //704x576 new
		frameW = 704;
		frameH = 576;
		break;
	case 922800: //1280x720 new
		frameW = 1280;
		frameH = 720;
		break;
	case 2074320: //1920x1080 new
		frameW = 1920;
		frameH = 1080;
		break;
	default:
		return -1;
	}
	*sb1_w = frameW / 3		+ !!(frameW % 3);
	*sb1_h = frameH / 3		+ !!(frameH % 3);
	*sb2_w = (*sb1_w) / 3 + !!((*sb1_w) % 3);
	*sb2_h = (*sb1_h) / 3 + !!((*sb1_h) % 3);
	return 0;
}

sb_t defineSbsType(Subband *sbs, const Ints* sbNums) {
    int sbNum = sbNums->vals[0];
    sb_t t = sbs[sbNum].sbType; //type of first sb (reference)
    for (int sb_i = 1; sb_i < sbNums->len; ++sb_i) {
        sbNum = sbNums->vals[sb_i];
        if (sbs[sbNum].sbType != t) { //sb types are unequal
            return sb_mixed;
        }
    }
    return t;
}




#define NS_IN_SEC 1000000000

struct timespec timespec_sub(struct timespec end, struct timespec start) {
	struct timespec res;
	res.tv_sec = end.tv_sec - start.tv_sec;
	res.tv_nsec = end.tv_nsec - start.tv_nsec;
	if (res.tv_nsec < 0) {
		res.tv_sec -= 1;
		res.tv_nsec = res.tv_nsec + NS_IN_SEC;
	}
	return res;
}

void timespec_add(struct timespec *main, struct timespec addend) {
	main->tv_sec += addend.tv_sec;
	main->tv_nsec += addend.tv_nsec;
	if (main->tv_nsec >= NS_IN_SEC) {
		main->tv_sec += 1;
		main->tv_nsec -= NS_IN_SEC;
	}
}

double timespec_double(struct timespec t) {
	return t.tv_sec + (double)t.tv_nsec / NS_IN_SEC;
}
