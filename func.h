#pragma once

#include <stdlib.h>
#include <math.h>

#include "constsAndTypes.h"
#include "ans.h"

int probPrec();
#define GET_STATE_PRECISION(_PROBABILITY_PRECISION)  (_PROBABILITY_PRECISION + 1)
#define GET_MAX_STATE(_STATE_PRECISION)  ((1 << _STATE_PRECISION) - 1)

double entropy(int *freqs, int freqsLen);
int checkFilesEquality(char *file1, char *file2);
int bitlen(uint64_t D);
void addBitToBits(Bits *buf, int bit);

int compareCoefFiles(FILE *fp1, FILE *fp2);
int compareCoefFiles_names(char *fileName1, char *fileName2);

//FileContent
int readFileContent(FILE *fp, FileContent *fc);
int compareFileContents(FileContent *fc1, FileContent *fc2);
void writeFileContent(FileContent *fc, FILE *fp);
void writeFileContent_name(char *fileName, FileContent *fc);
void freeFileContent(FileContent *fc);
void sepEsMants(Group *gr, Subband *sbs, md_t mode, Bytes *mants);
void sepEsMants_sb(Subband *sb, md_t mode, Bytes *mants);

void appointGroups(ExtMode *md, int _case);
void freeMdGroups(ExtMode *md);
int getNumOfContTypes(contType_t contType);
void createContMap_equal(ExtMode *md, contBase_t base, contType_t type, int isParentUsage, contType_t pType);
void createContMap_individual(ExtMode *md, contBase_t base, contType_t *types, int isParentUsage, contType_t pType);
void freeContMap(ExtMode *md);
int getNumOfConts(ContModel cm);
void makeNums_gr(Group gr, Subband *sbs, Ints *nums, Ints *contFlags, Dict* dicts);
void makeNums_sb(Subband *sb, Ints *nums, Ints *contFlags, Dict* dicts);
void changeNums(Ints *nums, const Ints* contFlags, int **mappingOldToNew);

//headers
int getGrSize(Group *gr, Subband *sbs);
void writeHeader(Bytes *out, const ExtMode *md, int totalSize, const Dict* dicts, const Dict* normDicts, int nDicts, int *hBits);
void readHeader(Bytes *bt, int remainingSize, Dict* dicts, int nDicts);

//subband, group
Subband emptySubband();
void freeSb(Subband *sb);
int getSubbandsSizes(uint64_t size, int *subband2_size, int *subband1_size);
int getSubbandsWH(uint64_t size, int *subband2_w, int *subband2_h, int *subband1_w, int *subband1_h);
sb_t defineSbsType(Subband *sbs, const Ints* sbNums);


//Matrix
Matrix emptyMatrix();
Matrix createMatrixWH(int w, int h);
Matrix createMatrixFromInts(const Ints* syms, int w, int h);
void freeMartix(Matrix * m);

int getContext(Matrix m, int row, int col, ContModel cm);
void considerParent(int *cont, Subband *sb, int row, int col, contType_t pType);
void considerParent_dec(int *cont, Subband *sb, int row, int col, contType_t pType, md_t mode);

//Dict
double entropy_dict(Dict* dict);
Dict allocDictLen(int len);
Dict* allocEmptyDicts(int nDicts);
Dict makeNormDict(const Dict* dict, const int PROBABILITY_PRECISION);
void copyDict(const Dict* src, Dict* dst);
int compSyms_desc(const void* a, const void* b);
int compSyms_asc(const void* a, const void* b);
void sortDict_getOrder(Dict* dict);
Dict* makeNormalizedDicts(Dict* dicts, int nConts);
Dict* makeNormalizedDicts_decEnv(Dict* dicts, int nDicts, const ExtMode* md);
Dict* makeSortedDicts(Dict* dicts, int nDicts, int ***mapOldToNew);
void renumberSyms(Dict* dicts, int nDicts, Ints* nums, const Ints* contFlags, const ExtMode *md, int nConts);
void freeDictsAr(Dict** dicts, int nDicts);

int nextSymVal(int sym);


//time
// double timediff(struct timespec end, struct timespec start);
struct timespec timespec_sub(struct timespec end, struct timespec start);
void timespec_add(struct timespec *main, struct timespec addend);
double timespec_double(struct timespec t);