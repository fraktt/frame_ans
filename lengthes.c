#include "lengthes.h"



void printLengthes(Lengthes len, const ExtMode* md, int num, char type) {
	if (num == 0) { //first line
		printf("\theader\t");
		if (md->mode == mode_coefs) {
			printf("payload\t");
		} else if (md->mode == mode_exps || md->mode == mode_exps2 || md->mode == mode_exps3) {
			printf("encData\tmants\t");
		}
		printf("SUM\n");
	} 

	if (type == 'g') { //groups
		printf("gr[%i]:\t", num);
	} 
	else /*if (type == 's')*/ { //individual subbands
		printf("sb[%i]:\t", num);
	}

	// printf("header = %-3i  ", len.header);
	// if (md->mode == mode_coefs) {
	// 	printf("payload = %-5i  %i\n", len.enc, len.header + len.enc);
	// } else if (md->mode == mode_exps || md->mode == mode_exps2 || md->mode == mode_exps3) {
	// 	printf("encData = %-5i  mants = %-5i  %i\n", len.enc, len.mants, len.header + len.enc + len.mants);
	// }	

	printf("%-3i\t", len.header);
	if (md->mode == mode_coefs) {
		printf("%-5i\t%i\n", len.enc, len.header + len.enc);
	} else if (md->mode == mode_exps || md->mode == mode_exps2 || md->mode == mode_exps3) {
		printf("%-5i\t%-5i\t%i\n", len.enc, len.mants, len.header + len.enc + len.mants);
	}
}

void printResultLengthes(Lengthes allLen, const ExtMode* md) {
	printf("Headers:\t%i bytes (dictlens=%i; syms=%i; freqs=%i; encDataLens=%i)\n", allLen.header, allLen.hBits[0], allLen.hBits[1], allLen.hBits[2], allLen.hBits[3]);
	
	if (md->mode == mode_coefs) {
		printf("Payload:\t%i bytes\n", allLen.enc);
		if (md->contBase == base_noCont) {
			printf("Payload limit:\t%i bytes\n", allLen.encLim);
		}
	} else if (md->mode == mode_exps || md->mode == mode_exps2 || md->mode == mode_exps3) {
		printf("Enc data:\t%i bytes\n", allLen.enc);
		if (md->contBase == base_noCont) {
			printf("Enc data limit:\t%i bytes\n", allLen.encLim);
		}
		printf("Mants:\t\t%i bytes\n", allLen.mants);
	}

	printf("SUM:\t\t%i bytes\n", allLen.header + allLen.enc + allLen.mants);
	if (md->contBase == base_noCont) {
		printf("SUM limit:\t%i bytes\n", allLen.header + allLen.encLim + allLen.mants);
	}
}


Lengthes sumLengthes(Lengthes len1, Lengthes len2) {
	Lengthes res = { 0 };
	res.abs = len1.abs + len2.abs;
	res.enc = len1.enc + len2.enc;
	res.encLim = len1.encLim + len2.encLim;
	res.signs = len1.signs + len2.signs;
	res.mants = len1.mants + len2.mants;
	res.all = len1.all + len2.all;
	res.allLim = len1.allLim + len2.allLim;
	res.header = len1.header + len2.header;
	for (int i = 0; i < 4; ++i) {
		res.hBits[i] = len1.hBits[i] + len2.hBits[i];
	}
	return res;
}






