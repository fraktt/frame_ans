#pragma once

#include "constsAndTypes.h"

typedef struct {
	int header;
	int enc;
	int encLim;
	int abs;
	int signs;
	int mants;
	int all;
	int allLim;
	int hBits[4];
} Lengthes;

void printLengthes(Lengthes len, const ExtMode* md, int num, char type);
Lengthes sumLengthes(Lengthes allLengthes, Lengthes len);
void printResultLengthes(Lengthes allLen, const ExtMode* md);



