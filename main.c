#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "constsAndTypes.h"
#include "func.h"
#include "encdec.h"



int main() {
	char inputFileName[] = "1080p_pedestrian_area.yuv_1920_1080_74_16_NonCont";
	// char inputFileName[] = "city_4cif.yuv_704_576_2_13_NonCont";
	// char inputFileName[] = "720p5994_parkrun_ter.yuv_1280_720_113_12_NonCont";
	
	char encFileName[256];
	strcpy(encFileName, inputFileName);
	strcat(encFileName, ".ans");
	char decFileName[256] = "decoded_";
	strcat(decFileName, inputFileName);
	printf("inputFileName: %s\n", inputFileName); //print filename
	printf("encFileName: %s\n", encFileName); //print filename
	printf("decFileName: %s\n", decFileName); //print filename
	printf("\n");

	//reading file, copying to array in memory
	FileContent ifc, ofc; //contents of input and output files
	FILE *ifp, *ofp; //input and output file pointers
	if (!(ifp = fopen(inputFileName, "r"))) {
		printf("Cannot open file %s\n", inputFileName);
		return 1;
	}
	readFileContent(ifp, &ifc);
	fclose(ifp);


	ExtMode md;
	memset(&md, 0, sizeof(md));
	md.fileName = "dicts.txt";
	md.alg = alg_ans; //alg_ans, alg_fse
	md.mode = mode_exps2; //mode_coefs, mode_exps, mode_exps2, mode_exps3
	md.dir = dir_forw;
	md.headerNorm = hf_plain; //hf_plain, hf_norm
	md.headerSort = hs_sortAbs; //hs_noSort, hs_sortFreq, hs_sortAbs
	md.precType = prec_const; //prec_const, prec_adapt(not working)

	md.contModelsAreDifferent = 0;
	md.contBase = base_LT; /*base_noCont, base_L, base_D, base_T, base_R, base_LD, base_DT, base_LT, base_LR, base_DR, base_LDT, base_LTR, base_LDTR,
								base_sumLT, base_sumLDTR, base_sumLL2, base_sumTT2, base_L2T2,
								base_sumLL2sumTT2, base_LL2TT2, base_TsumLL2, base_TLL2, base_LsumTT2, base_LTT2, base_LsumDT, base_LsumDTR, base_TsumLD, 
								base_LTP*/
	md.contType = n_z_p; //z_nz, n_z_p, n_n1_z_p1_p, z_a1_a, ez_nz, ez2_nz, n_n12_z_p12_p, z_a12_a, n_ez_p, n_ez2_p
	
	md.useGroups = 1;
	appointGroups(&md, 1);
	md.parentUsage = 0; //1 - use parents, 0 - don't use
	md.pType = n_z_p; //parent cont type //z_nz, ez_nz, ez2_nz, n_z_p, z_a1_a, n_n1_z_p1_p

	Times times;
	memset(&times, 0, sizeof(Times));
	md.times = &times;

	struct timespec start, end;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &start);

	// for (md.contType = z_nz; md.contType <= z_a1_a; ++md.contType) {
	// for (md.contBase = base_L; md.contBase <= base_LDT; ++md.contBase) {
	// for (int i = 0; i < 10; ++i) { 
		createContMap_equal(&md, md.contBase, md.contType, md.parentUsage, md.pType);
		//contType_t types[] = {n_z_p, z_nz}; ////
		//createContMap_individual(&md, md.contBase, types, md.parentUsage, md.pType); ////
		
		printInfo(&md);
		
		//encoding
		Bytes encBytes = emptyBytes();
		encFrame_sb(&ifc, &encBytes, &md);
		
		//decoding
		rewindBytes(&encBytes);
		decFrame_sb(&encBytes, ifc.total, &ofc, &md);
		
		freeBytes(&encBytes);

		compareFileContents(&ifc, &ofc);
		freeFileContent(&ofc);
		freeContMap(&md);

		printf("\n");
	// }
	// }
	// }
	

	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &end);

	printf("total time = %.9lf\n", timespec_double(timespec_sub(end, start)));
	
	printf("time_enc_createDirectTable = %.9lf\n", timespec_double(md.times->time_enc_createDirectTable));
	printf("time_enc_proc = %.9lf\n",  			   timespec_double(md.times->time_enc_proc));
	printf("time_enc_sepEsMants = %.9lf\n",  			  	timespec_double(md.times->time_enc_sepEsMants));
	printf("time_enc_makeNums = %.9lf\n",  			  		timespec_double(md.times->time_enc_makeNums));
	printf("time_enc_renumSyms = %.9lf\n",  			  	timespec_double(md.times->time_enc_renumSyms));
	printf("time_enc_normalizeDicts = %.9lf\n",  			timespec_double(md.times->time_enc_normalizeDicts));
	printf("time_enc_writeHeader = %.9lf\n",  			  	timespec_double(md.times->time_enc_writeHeader));
	printf("time_encSb = %.9lf\n",				   timespec_double(md.times->time_encSb));

	printf("time_dec_createDirectTable = %.9lf\n", 	timespec_double(md.times->time_dec_createDirectTable));
	printf("time_dec_createInverseTable = %.9lf\n", timespec_double(md.times->time_dec_createInverseTable));
	printf("time_dec_proc = %.9lf\n", 			   	timespec_double(md.times->time_dec_proc));
	printf("time_decSb = %.9lf\n",				  	timespec_double(md.times->time_decSb));


	freeFileContent(&ifc);
	freeMdGroups(&md);

	return 0;
}
