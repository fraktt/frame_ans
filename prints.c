#include "prints.h"
#include "func.h"


void printFreqs(int *freqs, int nSymbols) {
	printf("--- Frequencies of symbols ---\n");
	for (int i = 0; i < nSymbols; ++i) {
		if (freqs[i] > 0) {
			printf("%i: %i\n", i, freqs[i]);
		}
	}
	printf("\n");
}

void printEzcsFreq(int *freq, int nSymbols) {
	printf("--- int ezcsFreq[%i] ---\n", nSymbols);
	for (int i = 0; i < nSymbols; ++i) {
		printf("%i: %i\n", i, freq[i]);
	}
	printf("\n");
}

void printOrder(int *order, int nSymbols) {
	printf("--- int order[%i] ---\n", nSymbols);
	for (int i = 0; i < nSymbols; ++i) {
		printf("%i: %i\n", i, order[i]);
	}
	printf("\n");
}

void printChart(int *chart, int *sizes, int max_state, int nSymbols) {
	printf("--- printChart ---\n");

	printf("chart:\n");
	for (int i = 0; i <= max_state; ++i) {
		printf("%i: %i\n", i, chart[i]);
	}
	printf("\n");

	printf("sizes:\n");
	for (int i = 0; i < nSymbols; ++i) {
		printf("%i: %i\n", i, sizes[i]);
	}
	printf("\n");
}

void printDirectTable(int **directTable, int *sizes, int  nSymbols) {
	printf("--- printDirectTable ---\n");

	//simple printing
	for (int i = 0; i < nSymbols; ++i) {
		printf("%i: ", i);
		for (int j = 0; j < sizes[i]; j++) {
			printf("%i ", directTable[i][j]);
		}
		printf("\n");
	}


	//searching max size
	int maxSize = 0;
	for (int i = 0; i < nSymbols; ++i) {
		if (sizes[i] > maxSize) {
			maxSize = sizes[i];
		}
	}

	//table printing
	printf("\n");
	printf("state:\t");
	for (int i = 0; i < nSymbols; ++i) {
		printf("%i:\t", i);
	}
	printf("\n");

	for (int j = 1; j < maxSize; j++) {
		printf("%i\t", j);
		for (int i = 0; i < nSymbols; ++i) {
			if (sizes[i] > j) {
				printf("%i\t", directTable[i][j]);
			}
			else {
				printf(" \t");
			}
		}
		printf("\n");
	}
	printf("\n");
}

void printInverseTable(InvTab *inverseTable, int nStates) {
	printf("--- printInverseTable ---\n");
	printf("x':\tx:\ts:\n");
	for (int i = 0; i < nStates; ++i) {
		printf("%i\t%i\t%i\n", i, inverseTable[i].state, inverseTable[i].symbol);
	}
}

//void printStatisticsToFile(FILE *statfp, Subband *sb, int nSubbands2, int nSubbands1) {
//	
//	//printing subbands 2 (little)
//	fprintf(statfp, "\n--- subbands 2 ---\n\n");
//	for (int i = 0; i < nSubbands2; ++i) {
//		fprintf(statfp, "subbands2[%i]:\n", i);
//		for (int j = 0; j < sb[i].numLen; j++) {
//			fprintf(statfp, "%i: %i\n", sb[i].num2sym[j], sb[i].num2freq[j]);
//		}
//	}
//	fprintf(statfp, "\n\n");
//
//	//printing subbands 1 (big)
//	fprintf(statfp, "\n--- subbands 1 ---\n\n");
//	for (int i = nSubbands2; i < nSubbands2 + nSubbands1; ++i) {
//		fprintf(statfp, "subbands1[%i]:\n", i - nSubbands2);
//		for (int j = 0; j < sb[i].numLen; j++) {
//			fprintf(statfp, "%i: %i\n", sb[i].num2sym[j], sb[i].num2freq[j]);
//		}
//	}
//	fprintf(statfp, "\n\n");
//
//}

//void printSignsStat(Subband *sb) {
//	int ones = 0;
//	for (int i = 0; i < sb->nSignsBits; ++i) {
//		ones += sb->signs[i];
//	}
//	printf("signs statistics:\n");
//	printf("0: %i\n1: %i\nall: %i", sb->nSignsBits - ones, ones, sb->nSignsBits);
//
//}


void printSymDict(Sym *dict, int dictLen) {
	printf("--- printSymDict ---\n");
	for (int i = 0; i < dictLen; ++i) {
		printf("%i: sym = %i, freq = %i\n", dict[i].num, dict[i].sym, dict[i].freq);
	}
}

void printDict(const Dict* dict) {
	printf("dict.len = %i;   dict.sumOfFreqs = %i\n", dict->len, dict->sumOfFreqs);
	for (int i = 0; i < dict->len; ++i) {
		printf("num %2i:   sym %3i,     freq = %5i\n", dict->syms[i].num, dict->syms[i].sym, dict->syms[i].freq);
	}
}

char* algName(alg_t alg) {
	switch (alg) {
	case alg_ans:	return "ans";
	case alg_fse:	return "fse";
	default:		return NULL;
	}
}
char* dirName(dir_t dir) {
	switch (dir) {
	case dir_forw:	return "forward";
	case dir_backw: return "backward";
	default:		return NULL;
	}
}
char* contBaseName(contBase_t contBase) {
	switch (contBase) {
	case base_noCont:		return "noCont";
	case base_L:			return "L";
	case base_D:			return "D";
	case base_T:			return "T";
	case base_R:			return "R";
	case base_LD:			return "LD";
	case base_DT:			return "DT";
	case base_LT:			return "LT";
	case base_LR:			return "LR";
	case base_DR:			return "DR";
	case base_LDT:			return "LDT";
	case base_LTR:			return "LTR";
	case base_LDTR:			return "LDTR";
	case base_sumLT:		return "sumLT";
	case base_sumLDTR:		return "sumLDTR";
	case base_sumLL2:		return "sumLL2";
	case base_sumTT2:		return "sumTT2";
	case base_L2T2:			return "L2T2";
	case base_sumLL2sumTT2: return "sumLL2sumTT2";
	case base_LL2TT2:		return "LL2TT2";
	case base_TsumLL2:		return "TsumLL2";
	case base_TLL2:			return "TLL2";
	case base_LsumTT2:		return "LsumTT2";
	case base_LTT2:			return "LTT2";
	case base_LsumDT:		return "LsumDT";
	case base_LsumDTR:		return "LsumDTR";
	case base_TsumLD:		return "TsumLD";
	default:				return NULL;
	}
}
char* contTypeName(contType_t contType) {
	switch (contType) {
	case z_nz:				return "z_nz";
	case n_z_p:				return "n_z_p";
	case n_n1_z_p1_p:		return "n_n1_z_p1_p";
	case z_a1_a:			return "z_a1_a";
	case ez_nz:				return "ez_nz";
	case ez2_nz:			return "ez2_nz";
	case n_n12_z_p12_p:		return "n_n12_z_p12_p";
	case z_a12_a:			return "z_a12_a";
	case n_ez_p:			return "n_ez_p";
	case n_ez2_p:			return "n_ez2_p";
	default:				return NULL;
	}
}
char* headerNormName(headerNorm_t headerNorm) {
	switch (headerNorm) {
	case hf_plain:		return "plain";
	case hf_norm:		return "norm";
	default:			return NULL;
	}
}
char* headerSortName(headerSort_t headerSort) {
	switch (headerSort) {
	case hs_noSort:		return "noSort";
	case hs_sortFreq:	return "sortFreq";
	case hs_sortAbs:	return "sortAbs";
	default:			return NULL;
	}
}

 
void writeSbDictsLensToFile(FILE *fp, int sbNum, Dict* dicts, int nDicts) { //for Ckn
	fprintf(fp, "%i: ", sbNum);
	for (int i = 0; i < nDicts; ++i) {
		fprintf(fp, "%i ", dicts[i].len);
	}
	fprintf(fp, "\n");
}



void printDictsToFile(FILE *fp, Dict* dicts, int nDicts) {
	fprintf(fp, "nDicts = %i\n", nDicts);
	fprintf(fp, "\n");
	for (int i = 0; i < nDicts; ++i) {
		fprintf(fp, "dicts[%i]:\n", i);
		fprintf(fp, "len = %i\n", dicts[i].len);
		fprintf(fp, "sumOfFreqs = %i\n", dicts[i].sumOfFreqs);
		fprintf(fp, "num sym freq\n");
		for (int j = 0; j < dicts[i].len; ++j) {
			fprintf(fp, "%i %i %i\n", dicts[i].syms[j].num, dicts[i].syms[j].sym, dicts[i].syms[j].freq);
		}
		fprintf(fp, "\n");
	}
	fprintf(fp, "\n");
}

void printDictsToFile_forMatlab(FILE *fp, Dict* dicts, int nDicts) {
	fprintf(fp, "%i\n", nDicts);
	for (int i = 0; i < nDicts; ++i) {
		fprintf(fp, "%i\n", dicts[i].len);
		fprintf(fp, "%i\n", dicts[i].sumOfFreqs);
		for (int j = 0; j < dicts[i].len; ++j) {
			fprintf(fp, "%i %i %i\n", dicts[i].syms[j].num, dicts[i].syms[j].sym, dicts[i].syms[j].freq);
		}
	}
}

void printInfo(const ExtMode* md) {
	int contsAreEqual = 1; //true
	for (int i = 0; i < md->nSbGroups; ++i) {
		//if ((md->contMap[i].base < base_noCont) || (md->contMap[i].base > base_L2T2) ||
		//	(md->contMap[i].type < z_nz)        || (md->contMap[i].type > z_a12_a)) {
		//	printf("BAD CONT\n");
		//	exit(1);
		//}
		/*if ((md->contMap[i].base != md->contMap[0].base) || (md->contMap[i].type != md->contMap[0].type)) {
			contsAreEqual = 0; //false
		}*/
	}
	printf("PREC=%i; ", probPrec());
	int numOfConts = md->contModelsAreDifferent ? getNumOfConts(md->contMap[0]) : getNumOfConts(md->uniformContModel);
	if (contsAreEqual) {
		printf("contType:%s; contBase:%s; nConts=%i; parentUsage=%i; pType:%s\n", 
				contTypeName(md->contType), contBaseName(md->contBase), numOfConts, md->parentUsage, contTypeName(md->pType));
	}
	else {
		printf("cont:different;\n");
	}
	printf("hNorm:%s; hSort:%s\n", headerNormName(md->headerNorm), headerSortName(md->headerSort));

}
