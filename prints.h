#pragma once

#include <stdio.h>
#include "constsAndTypes.h"



void printFreqs(int *freq, int nSymbols);

void printEzcsFreq(int * freq, int nSymbols);

void printOrder(int *order, int nSymbols);

void printChart(int *chart, int *sizes, int max_state, int nSymbols);

void printDirectTable(int **directTable, int *sizes, int  nSymbols);

void printInverseTable(InvTab *inverseTable, int nStates);

//void printStatisticsToFile(FILE *statfp2, Subband *sb, int nSubbands2, int nSubbands1);

void printSymDict(Sym *dict, int dictLen);

void printDict(const Dict* dict);

char* algName(alg_t alg);
char* dirName(dir_t dir);
char* contBaseName(contBase_t contBase);
char* contTypeName(contType_t contType);
char* headerNormName(headerNorm_t headerNorm);

void writeSbDictsLensToFile(FILE *fp, int sbNum, Dict* dicts, int nDicts);

void printDictsToFile(FILE *fp, Dict* dicts, int nDict);
void printDictsToFile_forMatlab(FILE *fp, Dict* dicts, int nDict);

void printInfo(const ExtMode* md);